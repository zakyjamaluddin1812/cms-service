<?php

namespace App\Http\Controllers;

use App\Models\Akun as ModelsAkun;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class Akun extends Controller
{
    public function index()
    {
        if (!isset($_COOKIE['akun_id'])) return redirect('/login');
        $id_akun = $_COOKIE['akun_id'];
        $user = ModelsAkun::find($id_akun);

        $akun = ModelsAkun::all();
        return view('dashboard.akun', [
            "akun" => $akun,
            "user" => $user
        ]);
    }

    public function postAkun(Request $request)
    {
        $akun = new ModelsAkun();
        $nama = $request->post('nama');
        $password = $request->post('password');
        $akun->nama = $nama;
        $akun->password = $password;
        $akun->save();

        return response()->json([
            "message" => "post sucess",
            "id"   => $akun->id
        ]);
    }

    public function putAkun(Request $request, $id)
    {
        $akun = ModelsAkun::find($id);
        $nama = $request->all('nama')['nama'];
        $password = $request->all('password')['password'];
        $passwordLama = $request->all('passwordLama')['passwordLama'];
        if ($passwordLama !== $akun->password) abort(401, "Password lama tidak sesuai");
        $akun->nama = $nama;
        $akun->password = $password;
        $akun->save();

        return response()->json([
            "message" => "post sucess",
            "id"   => $akun->id
        ]);
    }

    public function deleteAkun($id)
    {
        ModelsAkun::where('id', $id)->delete();
        return response()->json([
            "message" => "data berhasil dihapus"
        ]);
    }
}
