<?php

namespace App\Http\Controllers;

use App\Models\Asrama as ModelsAsrama;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use App\Models\Akun as ModelsAkun;
use Illuminate\Support\Facades\Storage;

class Asrama extends Controller
{
    public function index()
    {
        if (!isset($_COOKIE['akun_id'])) return redirect('/login');
        $id_akun = $_COOKIE['akun_id'];
        $user = ModelsAkun::find($id_akun);

        $asrama = ModelsAsrama::all();
        return view('dashboard.asrama', [
            "asrama" => $asrama,
            "user" => $user
        ]);
    }

    public function putTitle(Request $request, $id)
    {
        $asrama = ModelsAsrama::find($id);
        $asrama->title = $request->all('data')['data'];
        $asrama->save();

        return response()->json([
            "message" => "put title success"
        ]);
    }

    public function putSubtitle(Request $request, $id)
    {
        $asrama = ModelsAsrama::find($id);
        $asrama->subtitle = $request->all('data')['data'];
        $asrama->save();

        return response()->json([
            "message" => "put subtitle success"
        ]);
    }

    public function putLink(Request $request, $id)
    {
        $asrama = ModelsAsrama::find($id);
        $asrama->link = $request->all('data')['data'];
        $asrama->save();

        return response()->json([
            "message" => "put link success"
        ]);
    }

    public function putImage(Request $request, $id)
    {
        $image = $request->file('data');
        if ($image == null) return response()->json(["error" => "gambar tidak terunggah"]);
        if (strtolower($image->extension()) == 'jpg' || strtolower($image->extension()) == 'jpeg' || strtolower($image->extension()) == 'png') {
            $name = $image->hashName();
            $image->move(public_path('assets/img/asrama/'), $name);
            $asrama = ModelsAsrama::find($id);
            Storage::delete('assets/img/asrama/' . $asrama->icon);
            $asrama->icon = $name;
            $asrama->save();
            return response()->json([
                "message" => "edit image success"
            ]);
        } else return response()->json(["error" => "file tidak valid"]);
    }

    public function postAsrama(Request $request)
    {
        $asrama = new ModelsAsrama();
        $title = $request->post('title');
        $content = explode(",", $request->post('content'));
        $subtitle = $content[0];
        $link = $content[1];
        $icon = $request->file('image');
        if ($icon == null) return response()->json(["error" => "gambar tidak terunggah"]);
        if (strtolower($icon->extension()) == 'jpg' || strtolower($icon->extension()) == 'jpeg' || strtolower($icon->extension()) == 'png') {
            $name = $icon->hashName();
            $icon->move(public_path('assets/img/asrama/'), $name);
            $asrama->icon = $name;
            $asrama->title = $title;
            $asrama->link = $link;
            $asrama->subtitle = $subtitle;
            $asrama->save();

            return response()->json([
                "message" => "asrama berhasil di tambahkan",
                "id"   => $asrama->id
            ]);
        } else return response()->json(["error" => "file tidak valid"]);
    }

    public function deleteAsrama($id)
    {
        $asrama = ModelsAsrama::find($id);
        if ($asrama->icon != null) Storage::delete('assets/img/asrama/' . $asrama->icon);
        ModelsAsrama::where('id', $id)->delete();
        return response()->json([
            "message" => "delete success"
        ]);
    }
}
