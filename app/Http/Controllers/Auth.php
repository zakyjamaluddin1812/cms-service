<?php

namespace App\Http\Controllers;

use App\Models\Akun;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Auth extends Controller
{
    public function index()
    {
        if (isset($_COOKIE['akun_id'])) return redirect('/');
        return view('login');
    }

    public function login(Request $request)
    {
        $akun = Akun::where('nama', $request->post('nama'))->first();
        if ($akun == null) return response()->json(["error" => "nama salah"], 401);
        if ($akun->password !== $request->post('password')) return response()->json(["error" => "sandi salah"], 401);
        return response()->json(["message" => "sukses"])->cookie('akun_id', $akun->id, 1440)->cookie('token', md5(time() . $akun->id), 1440);
    }

    public function logout()
    {
        if (isset($_COOKIE['akun_id'])) {
            unset($_COOKIE['akun_id']);
            setcookie('akun_id', null, -1, '/');
            unset($_COOKIE['token']);
            setcookie('token', null, -1, '/');
            return redirect('/login');
        } else {
            return redirect()->back();
        }
    }
}
