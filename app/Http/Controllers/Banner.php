<?php

namespace App\Http\Controllers;

use App\Models\Banner as ModelsBanner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use App\Models\Akun as ModelsAkun;
use Illuminate\Support\Facades\Storage;

class Banner extends Controller
{
    public function index()
    {
        if (!isset($_COOKIE['akun_id'])) return redirect('/login');
        $id_akun = $_COOKIE['akun_id'];
        $banner = ModelsBanner::first();
        $user = ModelsAkun::find($id_akun);

        return view('dashboard.banner', [
            'banner' => $banner,
            'user' => $user
        ]);
    }

    public function putTitle(Request $request, $id)
    {
        $all = $request->all();
        $title = $all["data"];
        $banner = ModelsBanner::find($id);
        $banner->title = $title;
        $banner->save();

        return response()->json([
            "message" => "Update success"
        ]);
    }

    public function putDesc(Request $request, $id)
    {
        $all = $request->all();
        $title = $all["data"];
        $banner = ModelsBanner::find($id);
        $banner->subtitle = $title;
        $banner->save();

        return response()->json([
            "message" => "Update success"
        ]);
    }

    public function putImage(Request $request)
    {
        $image = $request->file('data');
        if ($image == null) return response()->json(["error" => "gambar tidak terunggah"]);
        if (strtolower($image->extension()) == 'jpg' || strtolower($image->extension()) == 'jpeg' || strtolower($image->extension()) == 'png') {

            $name = $image->hashName();
            $image->move(public_path('assets/img/banner/'), $name);
            $banner = ModelsBanner::find(1);
            Storage::delete('assets/img/banner/' . $banner->image);
            $banner->image = $name;
            $banner->save();
            return response()->json([
                "message" => "success"
            ]);
        } else return response()->json(["error" => "file tidak sesuai"]);
    }
}
