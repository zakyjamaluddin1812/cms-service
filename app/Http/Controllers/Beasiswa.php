<?php

namespace App\Http\Controllers;

use App\Models\Beasiswa as ModelsBeasiswa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use App\Models\Akun as ModelsAkun;
use Illuminate\Support\Facades\Storage;

class Beasiswa extends Controller
{
    public function index()
    {
        if (!isset($_COOKIE['akun_id'])) return redirect('/login');
        $id_akun = $_COOKIE['akun_id'];
        $user = ModelsAkun::find($id_akun);

        $beasiswa = ModelsBeasiswa::all();
        return view('dashboard.beasiswa', [
            "beasiswa" => $beasiswa,
            'user' => $user
        ]);
    }

    public function putImage(Request $request, $id)
    {
        $image = $request->file('data');
        if ($image == null) return response()->json(["error" => "gambar tidak terunggah"]);
        if (strtolower($image->extension()) == 'jpg' || strtolower($image->extension()) == 'jpeg' || strtolower($image->extension()) == 'png') {
            $name = $image->hashName();
            $image->move(public_path('assets/img/beasiswa/'), $name);
            $beasiswa = ModelsBeasiswa::find($id);
            Storage::delete('assets/img/beasiswa/' . $beasiswa->image);
            $beasiswa->image = $name;
            $beasiswa->save();
            return response()->json([
                "message" => "success"
            ]);
        } else return response()->json(["error" => "file tidak valid"]);
    }

    public function putTitle(Request $request, $id)
    {
        $title = $request->all("data")["data"];
        $beasiswa = ModelsBeasiswa::find($id);
        $beasiswa->title = $title;
        $beasiswa->save();

        return response()->json([
            "message" => "data berhasil tersimpan"
        ]);
    }

    public function putContent(Request $request, $id)
    {
        $content = $request->all("data")["data"];
        $beasiswa = ModelsBeasiswa::find($id);
        $beasiswa->content = $content;
        $beasiswa->save();

        return response()->json([
            "message" => "data berhasil tersimpan"
        ]);
    }

    public function deleteBeasiswa($id)
    {
        $beasiswa = ModelsBeasiswa::find($id);
        Storage::delete('assets/img/beasiswa/' . $beasiswa->image);
        ModelsBeasiswa::where('id', $id)->delete();
        return response()->json([
            "message" => "delete sucess"
        ]);
    }

    public function postBeasiswa(Request $request)
    {
        $beasiswa = new ModelsBeasiswa();
        $title = $request->post('title');
        $content = $request->post('content');
        $image = $request->file('image');
        if ($image == null) return response()->json(["error" => "gambar tidak terunggah"]);
        if (strtolower($image->extension()) == 'jpg' || strtolower($image->extension()) == 'jpeg' || strtolower($image->extension()) == 'png') {
            $name = $image->hashName();
            $image->move(public_path('assets/img/beasiswa'), $name);
            $beasiswa->image = $name;
            $beasiswa->title = $title;
            $beasiswa->content = $content;
            $beasiswa->save();

            return response()->json([
                "message" => "add beasiswa sucess",
                "id"   => $beasiswa->id
            ]);
        } else return response()->json(["error" => "file tidak valid"]);
    }
}
