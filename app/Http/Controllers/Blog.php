<?php

namespace App\Http\Controllers;

use App\Models\Blog as ModelsBlog;
use App\Models\BlogContent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use App\Models\Akun as ModelsAkun;
use Illuminate\Support\Facades\Storage;

    class Blog extends Controller
{
    public function index()
    {
        if (!isset($_COOKIE['akun_id'])) return redirect('/login');
        $id_akun = $_COOKIE['akun_id'];
        $user = ModelsAkun::find($id_akun);

        $blog = ModelsBlog::all();
        return view('dashboard.blog', [
            "blog" => $blog,
            'user' => $user
        ]);
    }

    public function detail($id)
    {
        if (!isset($_COOKIE['akun_id'])) return redirect('/login');
        $id_akun = $_COOKIE['akun_id'];
        $user = ModelsAkun::find($id_akun);

        $blog = ModelsBlog::find($id);
        return view('dashboard.blog-detail', [
            "blog" => $blog,
            'user' => $user
        ]);
    }

    public function add()
    {
        if (!isset($_COOKIE['akun_id'])) return redirect('/login');
        $id_akun = $_COOKIE['akun_id'];
        $user = ModelsAkun::find($id_akun);

        return view('dashboard.blog-add', [
            'user' => $user
        ]);
    }

    public function putTitle(Request $request, $id)
    {
        $title = $request->all('data')['data'];
        $blog = ModelsBlog::find($id);
        $blog->title = $title;
        $blog->save();
        return response()->json([
            "message" => "put title success"
        ]);
    }

    public function putImage(Request $request, $id)
    {
        $image = $request->file('data');
        if ($image == null) return response()->json(["error" => "gambar tidak terunggah"]);
        if (strtolower($image->extension()) == 'jpg' || strtolower($image->extension()) == 'jpeg' || strtolower($image->extension()) == 'png') {
            $name = $image->hashName();
            $image->move('assets/img/blog', $name);

            $blog = ModelsBlog::find($id);
            Storage::delete('uploads' . $blog->image);
            $blog->thumbnail = $name;
            $blog->save();
            return response()->json([
                "message" => "put image success"
            ]);
        } else return response()->json(["error" => "file tidak valid"]);
    }

    public function putDeskripsi(Request $request, $id)
    {
        $blog = ModelsBlog::find($id);
        $blog->deskripsi = $request->all('data')['data'];
        $blog->save();
        return response()->json([
            "message" => "put deskripsi success"
        ]);
    }

    public function putContent(Request $request, $id)
    {
        $blog = ModelsBlog::find($id);
        $blog->content = $request->all('data')['data'];
        $blog->save();
        return response()->json([
            "message" => "put content success"
        ]);
    }


    public function deleteBlog($id)
    {

        $blog = ModelsBlog::find($id);
        Storage::delete('uploads' . $blog->image);
        ModelsBlog::where('id', $id)->delete();
        return response()->json([
            "message" => "delete blog success"
        ]);
    }

    public function postBlog(Request $request)
    {
        $author = ModelsAkun::find($_COOKIE['akun_id']);
        $blog = new ModelsBlog();
        $title = $request->post('title');
        $content = $request->post('content');
        $deskripsi = $request->post('deskripsi');
        $image = $request->file('image');
        if ($image == null) return response()->json(["error" => "gambar tidak terunggah"]);
        if (strtolower($image->extension()) == 'jpg' || strtolower($image->extension()) == 'jpeg' || strtolower($image->extension()) == 'png') {
            $name = $image->hashName();
            $image->move('assets/img/blog', $name);
            $blog->thumbnail = $name;
            $blog->title = $title;
            $blog->author = $author->nama;
            $blog->content = $content;
            $blog->deskripsi = $deskripsi;
            $blog->save();
            return response()->json([
                "message" => "blog berhasil tersimpan"
            ]);
        } else return response()->json(["error" => "file tidak valid"]);

    }

    public function upImage(Request $request)
    {
        $image = $request->file('data');
        if ($image == null) return response()->json(["error" => "gambar tidak terunggah"]);
        if (strtolower($image->extension()) == 'jpg' || strtolower($image->extension()) == 'jpeg' || strtolower($image->extension()) == 'png') {
            $name = $image->hashName();
            $image->move('assets/img/blog', $name);
            return response()->json([
                "message" => "success",
                "path" => "assets/img/blog/" . $name
            ]);
        } else return response()->json(["error" => "file tidak valid"]);
    }
}
