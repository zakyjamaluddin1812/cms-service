<?php

namespace App\Http\Controllers;

use App\Models\PpdbStatus;
use App\Models\Siswa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use App\Models\Akun as ModelsAkun;

class Formulir extends Controller
{
    public function index()
    {
        if (!isset($_COOKIE['akun_id'])) return redirect('/login');
        $id_akun = $_COOKIE['akun_id'];
        $user = ModelsAkun::find($id_akun);

        $status = PpdbStatus::first();
        $siswa = Siswa::all();
        return view('ppdb.index', [
            "status" => $status,
            "siswa" => $siswa,
            'user' => $user
        ]);
    }
    public function form()
    {
        if (!isset($_COOKIE['akun_id'])) return redirect('/login');
        $id_akun = $_COOKIE['akun_id'];
        $user = ModelsAkun::find($id_akun);

        return view('ppdb.form', [
            'user' => $user
        ]);
    }
    public function formEdit($id)
    {
        if (!isset($_COOKIE['akun_id'])) return redirect('/login');
        $id_akun = $_COOKIE['akun_id'];
        $user = ModelsAkun::find($id_akun);

        $siswa = Siswa::find($id);
        return view('ppdb.form-edit', [
            "siswa" => $siswa,
            'user' => $user
        ]);
    }

    public function putStatus(Request $request)
    {
        $statuses = PpdbStatus::first();
        $tahun_ajaran = $request->all("tahun")["tahun"];
        $status = $request->all("status")["status"];

        $statuses->tahun_ajaran = $tahun_ajaran;
        $statuses->status = $status;
        $statuses->save();


        return response()->json([
            "message" => "Status berhasil diubah"
        ]);
    }

    public function addSiswa(Request $request)
    {
        $siswa = new Siswa();
        $nama_lengkap = $request->post('nama_lengkap');
        $tempat_lahir = $request->post('tempat_lahir');
        $tanggal_lahir = $request->post('tanggal_lahir');
        $asal_sekolah = $request->post('asal_sekolah');
        $nisn = $request->post('nisn');
        $nik_siswa = $request->post('nik_siswa');
        $nomor_kk = $request->post('nomor_kk');
        $jenis_kelamin = $request->post('jenis_kelamin');
        $anak_ke = $request->post('anak_ke');
        $jumlah_saudara = $request->post('jumlah_saudara');
        $rt = $request->post('rt');
        $rw = $request->post('rw');
        $desa = $request->post('desa');
        $kecamatan = $request->post('kecamatan');
        $kabupaten = $request->post('kabupaten');
        $nama_ayah = $request->post('nama_ayah');
        $nik_ayah = $request->post('nik_ayah');
        $pekerjaan_ayah = $request->post('pekerjaan_ayah');
        $nama_ibu = $request->post('nama_ibu');
        $nik_ibu = $request->post('nik_ibu');
        $pekerjaan_ibu = $request->post('pekerjaan_ibu');
        $nama_wali = $request->post('nama_wali');
        $nik_wali = $request->post('nik_wali');
        $pekerjaan_wali = $request->post('pekerjaan_wali');
        $nomor_hp = $request->post('nomor_hp');


        $file_kk = $request->file('file_kk');
        $file_kk_name = '';
        if ($file_kk != null) {
            if (strtolower($file_kk->extension()) == 'pdf') {
                $file_kk_name = $file_kk->hashName();
                $file_kk->move('assets/file/kk', $file_kk_name);
            } else return response()->json(["error" => "file tidak valid"]);
        }

        $file_ijazah = $request->file('file_ijazah');
        $file_ijazah_name = '';
        if ($file_ijazah != null) {
            if (strtolower($file_ijazah->extension()) == 'pdf') {
                $file_ijazah_name = $file_ijazah->hashName();
                $file_ijazah->move('assets/file/ijazah', $file_ijazah_name);
            } else return response()->json(["error" => "file tidak valid"]);
        }

        $file_nisn = $request->file('file_nisn');
        $file_nisn_name = '';
        if ($file_nisn != null) {
            if (strtolower($file_nisn->extension()) == 'pdf') {
                $file_nisn_name = $file_nisn->hashName();
                $file_nisn->move('assets/file/nisn', $file_nisn_name);
            } else return response()->json(["error" => "file tidak valid"]);
        }

        $file_kps = $request->file('file_kps');
        $file_kps_name = '';
        if ($file_kps != null) {
            if (strtolower($file_kps->extension()) == 'pdf') {
                $file_kps_name = $file_kps->hashName();
                $file_kps->move('assets/file/kps', $file_kps_name);
            } else return response()->json(["error" => "file tidak valid"]);
        }

        $file_pkh = $request->file('file_pkh');
        $file_pkh_name = '';
        if ($file_pkh != null) {
            if (strtolower($file_pkh->extension()) == 'pdf') {
                $file_pkh_name = $file_pkh->hashName();
                $file_pkh->move('assets/file/pkh', $file_pkh_name);
            } else return response()->json(["error" => "file tidak valid"]);
        }

        $file_foto = $request->file('file_foto');
        $file_foto_name = '';
        if ($file_foto != null) {
            if (strtolower($file_foto->extension()) == 'jpg' || strtolower($file_foto->extension()) == 'jpeg' || strtolower($file_foto->extension()) == 'png') {
                $file_foto_name = $file_foto->hashName();
                $file_foto->move('assets/file/foto', $file_foto_name);
            } else return response()->json(["error" => "file tidak valid"]);
        }




        $guru_perekom = $request->post('guru_perekom');
        $siswa->tahun_ajaran = PpdbStatus::first()['tahun_ajaran'];
        $siswa->nama_lengkap = $nama_lengkap;
        $siswa->tempat_lahir = $tempat_lahir;
        $siswa->tanggal_lahir = $tanggal_lahir;
        $siswa->asal_sekolah = $asal_sekolah;
        $siswa->nisn = $nisn;
        $siswa->nik_siswa = $nik_siswa;
        $siswa->nomor_kk = $nomor_kk;
        $siswa->jenis_kelamin = $jenis_kelamin;
        $siswa->anak_ke = $anak_ke;
        $siswa->jumlah_saudara = $jumlah_saudara;
        $siswa->rt = $rt;
        $siswa->rw = $rw;
        $siswa->desa = $desa;
        $siswa->kecamatan = $kecamatan;
        $siswa->kabupaten = $kabupaten;
        $siswa->nama_ayah = $nama_ayah;
        $siswa->nik_ayah = $nik_ayah;
        $siswa->pekerjaan_ayah = $pekerjaan_ayah;
        $siswa->nama_ibu = $nama_ibu;
        $siswa->nik_ibu = $nik_ibu;
        $siswa->pekerjaan_ibu = $pekerjaan_ibu;
        $siswa->nama_wali = $nama_wali;
        $siswa->nik_wali = $nik_wali;
        $siswa->pekerjaan_wali = $pekerjaan_wali;
        $siswa->nomor_hp = $nomor_hp;
        $siswa->file_kk = $file_kk_name;
        $siswa->file_ijazah = $file_ijazah_name;
        $siswa->file_nisn = $file_nisn_name;
        $siswa->file_kps = $file_kps_name;
        $siswa->file_pkh = $file_pkh_name;
        $siswa->file_foto = $file_foto_name;
        $siswa->guru_perekom = $guru_perekom;

        $siswa->save();
        return redirect('ppdb');
    }

    public function putSiswa(Request $request)
    {
        $siswa = Siswa::find($request->post('id'));
        $nama_lengkap = $request->post('nama_lengkap');
        $tempat_lahir = $request->post('tempat_lahir');
        $tanggal_lahir = $request->post('tanggal_lahir');
        $asal_sekolah = $request->post('asal_sekolah');
        $nisn = $request->post('nisn');
        $nik_siswa = $request->post('nik_siswa');
        $nomor_kk = $request->post('nomor_kk');
        $jenis_kelamin = $request->post('jenis_kelamin');
        $anak_ke = $request->post('anak_ke');
        $jumlah_saudara = $request->post('jumlah_saudara');
        $rt = $request->post('rt');
        $rw = $request->post('rw');
        $desa = $request->post('desa');
        $kecamatan = $request->post('kecamatan');
        $kabupaten = $request->post('kabupaten');
        $nama_ayah = $request->post('nama_ayah');
        $nik_ayah = $request->post('nik_ayah');
        $pekerjaan_ayah = $request->post('pekerjaan_ayah');
        $nama_ibu = $request->post('nama_ibu');
        $nik_ibu = $request->post('nik_ibu');
        $pekerjaan_ibu = $request->post('pekerjaan_ibu');
        $nama_wali = $request->post('nama_wali');
        $nik_wali = $request->post('nik_wali');
        $pekerjaan_wali = $request->post('pekerjaan_wali');
        $nomor_hp = $request->post('nomor_hp');


        $file_kk = $request->file('file_kk');
        if ($file_kk != null) {
            if (strtolower($file_kk->extension()) == 'pdf') {
                $file_kk_name = $file_kk->hashName();
                $file_kk->move('assets/file/kk', $file_kk_name);
            } else return response()->json(["error" => "file tidak valid"]);
        } else {
            $file_kk_name = $siswa->getOriginal('file_kk');
        }

        $file_ijazah = $request->file('file_ijazah');
        if ($file_ijazah != null) {
            if (strtolower($file_ijazah->extension()) == 'pdf') {
                $file_ijazah_name = $file_ijazah->hashName();
                $file_ijazah->move('assets/file/ijazah', $file_ijazah_name);
            } else return response()->json(["error" => "file tidak valid"]);
        } else {
            $file_ijazah_name = $siswa->getOriginal('file_ijazah');
        }

        $file_nisn = $request->file('file_nisn');
        if ($file_nisn != null) {
            if (strtolower($file_nisn->extension()) == 'pdf') {
                $file_nisn_name = $file_nisn->hashName();
                $file_nisn->move('assets/file/nisn', $file_nisn_name);
            } else return response()->json(["error" => "file tidak valid"]);
        } else {
            $file_nisn_name = $siswa->getOriginal('file_nisn');
        }

        $file_kps = $request->file('file_kps');
        if ($file_kps != null) {
            if ($file_kps->extension() != 'pdf') {
                $file_kps_name = $file_kps->hashName();
                $file_kps->move('assets/file/kps', $file_kps_name);
            } else return response()->json(["error" => "file tidak valid"]);
        } else {
            $file_kps_name = $siswa->getOriginal('file_kps');
        }

        $file_pkh = $request->file('file_pkh');
        if ($file_pkh != null) {
            if ($file_pkh->extension() != 'pdf') {
                $file_pkh_name = $file_pkh->hashName();
                $file_pkh->move('assets/file/pkh', $file_pkh_name);
            } else return response()->json(["error" => "file tidak valid"]);
        } else {
            $file_pkh_name = $siswa->getOriginal('file_pkh');
        }

        $file_foto = $request->file('file_foto');
        if ($file_foto != null) {
            if (strtolower($file_foto->extension()) == 'jpg' || strtolower($file_foto->extension()) == 'jpeg' || strtolower($file_foto->extension()) == 'png') {
                $file_foto_name = $file_foto->hashName();
                $file_foto->move('assets/file/foto', $file_foto_name);
            } else return response()->json(["error" => "file tidak valid"]);
        } else {
            $file_foto_name = $siswa->getOriginal('file_foto');
        }



        $guru_perekom = $request->post('guru_perekom');
        $siswa->nama_lengkap = $nama_lengkap;
        $siswa->tempat_lahir = $tempat_lahir;
        $siswa->tanggal_lahir = $tanggal_lahir;
        $siswa->asal_sekolah = $asal_sekolah;
        $siswa->nisn = $nisn;
        $siswa->nik_siswa = $nik_siswa;
        $siswa->nomor_kk = $nomor_kk;
        $siswa->jenis_kelamin = $jenis_kelamin;
        $siswa->anak_ke = $anak_ke;
        $siswa->jumlah_saudara = $jumlah_saudara;
        $siswa->rt = $rt;
        $siswa->rw = $rw;
        $siswa->desa = $desa;
        $siswa->kecamatan = $kecamatan;
        $siswa->kabupaten = $kabupaten;
        $siswa->nama_ayah = $nama_ayah;
        $siswa->nik_ayah = $nik_ayah;
        $siswa->pekerjaan_ayah = $pekerjaan_ayah;
        $siswa->nama_ibu = $nama_ibu;
        $siswa->nik_ibu = $nik_ibu;
        $siswa->pekerjaan_ibu = $pekerjaan_ibu;
        $siswa->nama_wali = $nama_wali;
        $siswa->nik_wali = $nik_wali;
        $siswa->pekerjaan_wali = $pekerjaan_wali;
        $siswa->nomor_hp = $nomor_hp;
        $siswa->file_kk = $file_kk_name;
        $siswa->file_ijazah = $file_ijazah_name;
        $siswa->file_nisn = $file_nisn_name;
        $siswa->file_kps = $file_kps_name;
        $siswa->file_pkh = $file_pkh_name;
        $siswa->file_foto = $file_foto_name;
        $siswa->guru_perekom = $guru_perekom;

        $siswa->save();
        return redirect('ppdb');
    }

    public function deleteSiswa($id)
    {
        Siswa::where('id', $id)->delete();
        return response()->json([
            "message" => "Data berhasil dihapus"
        ]);
    }
}
