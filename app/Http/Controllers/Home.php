<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use App\Http\Resources\PostResource;
use App\Models\Test;
use App\Models\Akun as ModelsAkun;
use Illuminate\Support\Facades\Cookie;

class Home extends Controller
{
    public function index(Request $request)
    {
        if (!isset($_COOKIE['akun_id'])) return redirect('/login');
        $id_akun = $_COOKIE['akun_id'];
        $user = ModelsAkun::find($id_akun);

        return view('home', [
            'user' => $user
        ]);
    }
}
