<?php

namespace App\Http\Controllers;

use App\Models\Kesiswaan as ModelsKesiswaan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use App\Models\Akun as ModelsAkun;
use Illuminate\Support\Facades\Storage;

class Kesiswaan extends Controller
{
    public function index()
    {
        if (!isset($_COOKIE['akun_id'])) return redirect('/login');
        $id_akun = $_COOKIE['akun_id'];
        $user = ModelsAkun::find($id_akun);

        $kesiswaan = ModelsKesiswaan::all();
        return view('dashboard.kesiswaan', [
            "kesiswaan" => $kesiswaan,
            'user' => $user
        ]);
    }

    public function putImage(Request $request, $id)
    {
        $image = $request->file('data');
        if ($image == null) return response()->json(["error" => "gambar tidak terunggah"]);
        if (strtolower($image->extension()) == 'jpg' || strtolower($image->extension()) == 'jpeg' || strtolower($image->extension()) == 'png') {
            $name = $image->hashName();
            $image->move(public_path('assets/img/kesiswaan'), $name);
            $kesiswaan = ModelsKesiswaan::find($id);
            Storage::delete('assets/img/kesiswaan' . $kesiswaan->image);
            $kesiswaan->image = $name;
            $kesiswaan->save();
            return response()->json([
                "message" => "success"
            ]);
        } else return response()->json(["error" => "file tidak valid"]);
    }

    public function putTitle(Request $request, $id)
    {
        $title = $request->all("data")["data"];
        $kesiswaan = ModelsKesiswaan::find($id);
        $kesiswaan->title = $title;
        $kesiswaan->save();

        return response()->json([
            "message" => "data berhasil tersimpan"
        ]);
    }

    public function putContent(Request $request, $id)
    {
        $content = $request->all("data")["data"];
        $kesiswaan = ModelsKesiswaan::find($id);
        $kesiswaan->content = $content;
        $kesiswaan->save();

        return response()->json([
            "message" => "data berhasil tersimpan"
        ]);
    }

    public function deleteKesiswaan($id)
    {
        $kesiswaan = ModelsKesiswaan::find($id);
        Storage::delete('assets/img/kesiswaan' . $kesiswaan->image);
        ModelsKesiswaan::where('id', $id)->delete();
        return response()->json([
            "message" => "delete sucess"
        ]);
    }

    public function postKesiswaan(Request $request)
    {
        $kesiswaan = new ModelsKesiswaan();
        $title = $request->post('title');
        $content = $request->post('content');
        $image = $request->file('image');
        if ($image == null) return response()->json(["error" => "gambar tidak terunggah"]);
        if (strtolower($image->extension()) == 'jpg' || strtolower($image->extension()) == 'jpeg' || strtolower($image->extension()) == 'png') {
            $name = $image->hashName();
            $image->move(public_path('uploads'), $name);
            $kesiswaan->image = $name;
            $kesiswaan->title = $title;
            $kesiswaan->content = $content;
            $kesiswaan->save();

            return response()->json([
                "message" => "post sucess",
                "id"   => $kesiswaan->id
            ]);
        } else return response()->json(["error" => "file tidak valid"]);
    }
}
