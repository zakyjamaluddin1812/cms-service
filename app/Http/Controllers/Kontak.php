<?php

namespace App\Http\Controllers;

use App\Models\Kontak as ModelsKontak;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use App\Models\Akun as ModelsAkun;

class Kontak extends Controller
{
    public function index()
    {
        if (!isset($_COOKIE['akun_id'])) return redirect('/login');
        $id_akun = $_COOKIE['akun_id'];
        $user = ModelsAkun::find($id_akun);

        $kontak = ModelsKontak::all();
        return view('dashboard.kontak', [
            "kontak" => $kontak,
            'user' => $user
        ]);
    }

    public function putKey(Request $request, $id)
    {
        $key = $request->all("data")["data"];
        $kontak = ModelsKontak::find($id);
        $kontak->key = $key;
        $kontak->save();
        return response()->json([
            "message" => "edit key success"
        ]);
    }

    public function putValue(Request $request, $id)
    {
        $value = $request->all("data")["data"];
        $kontak = ModelsKontak::find($id);
        $kontak->value = $value;
        $kontak->save();
        return response()->json([
            "message" => "edit value success"
        ]);
    }

    public function putIcon(Request $request, $id)
    {
        $icon = $request->all("data")["data"];
        $kontak = ModelsKontak::find($id);
        $kontak->icon = $icon;
        $kontak->save();
        return response()->json([
            "message" => "edit icon success"
        ]);
    }

    public function deleteKontak($id)
    {
        ModelsKontak::where('id', $id)->delete();
        return response()->json([
            "message" => "delete success"
        ]);
    }

    public function postKontak(Request $request)
    {
        $kontak = new ModelsKontak();
        $icon = $request->post('icon');
        $key = $request->post('key');
        $value = $request->post('value');
        $kontak->icon = $icon;
        $kontak->key = $key;
        $kontak->value = $value;
        $kontak->save();
        return response()->json([
            "message" => "success",
            "data" => $request->all(),
            "id" => $kontak->id
        ]);
    }
}
