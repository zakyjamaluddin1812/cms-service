<?php

namespace App\Http\Controllers;

use App\Models\Akun;
use App\Models\Pesan as ModelsPesan;
use Illuminate\Http\Request;

class Pesan extends Controller
{
    public function index()
    {
        if (!isset($_COOKIE['akun_id'])) return redirect('/login');
        $id_akun = $_COOKIE['akun_id'];
        $user = Akun::find($id_akun);
        $pesan = ModelsPesan::all();
        return view('dashboard.pesan', [
            "pesan" => $pesan,
            'user' => $user
        ]);
    }
}
