<?php

namespace App\Http\Controllers;

use App\Models\ProgramUnggulan as ModelsProgramUnggulan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use App\Models\Akun as ModelsAkun;

class ProgramUnggulan extends Controller
{
    public function index()
    {
        if (!isset($_COOKIE['akun_id'])) return redirect('/login');
        $id_akun = $_COOKIE['akun_id'];
        $user = ModelsAkun::find($id_akun);
        $programUnggulan = ModelsProgramUnggulan::all();
        return view(
            'dashboard.program-unggulan',
            [
                "programUnggulan" => $programUnggulan,
                'user' => $user
            ]
        );
    }

    public function putTitle(Request $request, $id)
    {
        $title = $request->all("data")["data"];
        $program = ModelsProgramUnggulan::find($id);
        $program->title = $title;
        $program->save();
        return response()->json([
            "message" => "edit title success"
        ]);
    }

    public function putSubtitle(Request $request, $id)
    {
        $subtitle = $request->all("data")["data"];
        $program = ModelsProgramUnggulan::find($id);
        $program->subtitle = $subtitle;
        $program->save();
        return response()->json([
            "message" => "edit subtitle success"
        ]);
    }

    public function putIcon(Request $request, $id)
    {
        $icon = $request->all("data")["data"];
        $program = ModelsProgramUnggulan::find($id);
        $program->icon = $icon;
        $program->save();
        return response()->json([
            "message" => "edit icon success"
        ]);
    }

    public function deleteProgram($id)
    {
        ModelsProgramUnggulan::where('id', $id)->delete();
        return response()->json([
            "message" => "delete success"
        ]);
    }

    public function postProgram(Request $request)
    {
        $program = new ModelsProgramUnggulan();
        $icon = $request->post('icon');
        $title = $request->post('title');
        $subtitle = $request->post('subtitle');
        $program->icon = $icon;
        $program->title = $title;
        $program->subtitle = $subtitle;
        $program->save();
        return response()->json([
            "message" => "success",
            "data" => $request->all(),
            "id" => $program->id
        ]);
    }
}
