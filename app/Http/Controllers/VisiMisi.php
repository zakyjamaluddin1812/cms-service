<?php

namespace App\Http\Controllers;

use App\Models\VisiMisi as ModelsVisiMisi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use App\Models\Akun as ModelsAkun;

class VisiMisi extends Controller
{
    public function index()
    {
        if (!isset($_COOKIE['akun_id'])) return redirect('/login');
        $id_akun = $_COOKIE['akun_id'];
        $user = ModelsAkun::find($id_akun);

        $visi = ModelsVisiMisi::where('type', 'visi')->first();
        $misi = ModelsVisiMisi::where('type', 'misi')->get();
        return view('dashboard.visi-misi', [
            'visi' => $visi,
            'misi' => $misi,
            'user' => $user
        ]);
    }

    public function putVisi(Request $request, $id)
    {
        $visi = ModelsVisiMisi::find($id);
        $visi->value = $request->all("data")["data"];
        $visi->save();

        return response()->json([
            "message" => "success"
        ]);
    }

    public function addMisi(Request $request)
    {
        $value = $request->post("data");
        $misi = new ModelsVisiMisi();
        $misi->type = "misi";
        $misi->value = $value;
        $misi->save();

        $id = $misi->id;

        return response()->json([
            "message" => "add misi success",
            "id" => $id
        ]);
    }

    public function putMisi(Request $request, $id)
    {
        $misi = ModelsVisiMisi::find($id);
        $misi->value = $request->all("data")["data"];
        $misi->save();

        return response()->json([
            "message" => "success"
        ]);
    }

    public function deleteMisi($id)
    {
        ModelsVisiMisi::where('id', $id)->delete();

        return response()->json([
            "message" => "delete success"
        ]);
    }
}
