async function postData(url = '', data = {}) {
const response = await fetch(url, {
    method : 'POST',
    mode: 'cors', // no-cors, *cors, same-origin
    cache: 'no-cache',
    credentials: 'same-origin',
    headers: {
    'Content-Type': 'application/json',
    'token' : data.token
    // 'Content-Type': 'application/x-www-form-urlencoded',
    },
    redirect: 'follow', // manual, *follow, error
    referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
    body: JSON.stringify(data) // body data type must match "Content-Type" header
});
return response.json(); // parses JSON response into native JavaScript objects
}

async function getData(url = '') {
const response = await fetch(url, {
    method: 'GET',
    mode: 'cors',
    cache: 'no-cache',
    credentials: 'same-origin',
    headers: {
    'Content-Type': 'application/json'
    },
    redirect: 'follow',
    referrerPolicy: 'no-referrer',
});
return response.json();
}

async function putData(url = '', data = {}) {
    const response = await fetch(url, {
        method: 'PUT',
        mode: 'cors',
        cache: 'no-cache',
        credentials: 'same-origin',
        headers : {
            'Content-Type': 'application/x-www-form-urlencoded',
            'token' : data.token
        },
        redirect: 'follow',
        referrerPolicy: 'no-referrer',
        body : JSONToForm(data)
    });
    return response.json();
}

async function putImage(url = '', data = {}) {
    const formData = new FormData()
    formData.append("data", data.data)
    const response = await fetch(url, {
        method: 'POST',
        mode: 'cors',
        cache: 'no-cache',
        credentials: 'same-origin',
        headers: {
        // 'Content-Type': 'multipart/form-data'
            'token' : data.token
        },
        redirect: 'follow',
        referrerPolicy: 'no-referrer',
        body : formData
    });
    return response.json();
}

async function postMultipart (url = '', data = {})
{
    const formData = new FormData()

    formData.append("title", data.title)
    formData.append("content", data.content)
    formData.append("image", data.image)
    formData.append("deskripsi", data.deskripsi)
    const response = await fetch(url, {
        method: 'POST',
        mode: 'cors',
        cache: 'no-cache',
        credentials: 'same-origin',
        headers: {
        // 'Content-Type': 'multipart/form-data'
            'token' : data.token
        },
        redirect: 'follow',
        referrerPolicy: 'no-referrer',
        body : formData
    });
    return response.json();
}

async function deleteData(url = '', data) {
    const response = await fetch(url, {
        method: 'DELETE',
        mode: 'cors',
        cache: 'no-cache',
        credentials: 'same-origin',
        headers: {
        'Content-Type': 'application/json',
        'token' : data.token
        },
        redirect: 'follow',
        referrerPolicy: 'no-referrer',
    });
    return response.json();
}

const JSONToForm = (data) => {
    const form = Object.keys(data).map((key) => {
    return encodeURIComponent(key) + '=' + encodeURIComponent(data[key]);
    }).join('&')

    return form
}

const objectToFormData = function(obj, form, namespace) {

    var fd = form || new FormData();
    var formKey;

    for(var property in obj) {
      if(obj.hasOwnProperty(property)) {

        if(namespace) {
          formKey = namespace + '[' + property + ']';
        } else {
          formKey = property;
        }

        // if the property is an object, but not a File,
        // use recursivity.
        if(typeof obj[property] === 'object' && !(obj[property] instanceof File)) {

          objectToFormData(obj[property], fd, property);

        } else {

          // if it's a string or a File object
          fd.append(formKey, obj[property]);
        }

      }
    }

    return fd;

  };

  const show = (message = 'Terimakasih') => {
    Swal.fire({
        width: 'fit-content',
        height: 'fit-content',
        backdrop: false,
        position: 'bottom-start',
        background: '#000',
        color: '#fff',
        text: message,
        showConfirmButton: false,
        timer: 2000
    })
}

