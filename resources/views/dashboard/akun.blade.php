@extends('../template')
@section('title', 'Akun')
@section('main')
<main id="main" class="main">

    <div class="pagetitle">
        <h1>Akun</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                <li class="breadcrumb-item active">Akun</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="section dashboard">
        <div class="row" id="row-container">
            @foreach($akun as $as)
            <div class="col-lg-6" id="">
                <div class="card info-card sales-card">
                    <div class="card-body">
                        <h5 class="mt-4 d-flex justify-content-between align-items-center">
                            <span>
                                <span class="card-title" id="heading-{{$as->id}}">{{$as->nama}}</span>
                                <div id="spinner-{{$as->id}}" class="d-none spinner-border spinner-border-sm text-success mx-3" role="status"></div>
                            </span>
                            <span id="trash-{{$as->id}}" onclick="deleteConfirm(this)" class="float-right text-danger"><i class="bi bi-trash"></i></span>
                        </h5>
                        <div class="form-group mt-3">
                            <input type="text" class="form-control" id="nama-{{$as->id}}" aria-describedby="emailHelp" placeholder="Nama" value="{{$as->nama}}" onblur="putAkun(this.parentNode.parentNode)">
                            <div class="invalid-feedback">
                                Nama harus diisi
                            </div>
                        </div>
                        <div class="form-group mt-3">
                            <input type="password" class="form-control" id="password-{{$as->id}}" aria-describedby="emailHelp" placeholder="New Password" value="" onblur="putAkun(this.parentNode.parentNode)">
                            <div class="invalid-feedback">
                                Password harus diisi
                            </div>
                        </div>
                        <div class="form-group mt-3">
                            <input type="password" class="form-control" id="password-confirm-{{$as->id}}" aria-describedby="emailHelp" placeholder="Password Confirm" value="" onblur="putAkun(this.parentNode.parentNode)">
                            <div class="invalid-feedback">
                                Konfirmasi password harus sama
                            </div>
                        </div>
                        <div class="form-group mt-3">
                            <input type="password" class="form-control" id="password-lama-{{$as->id}}" aria-describedby="emailHelp" placeholder="Password Lama" value="" onblur="putAkun(this.parentNode.parentNode)">
                            <div class="invalid-feedback">
                                Password lama harus diisi
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            <div id="col-plus" style="min-height: 200px;" class="col-lg-6 d-flex justify-content-center align-items-center">
                <div class="card d-flex justify-content-center align-items-center rounded-circle add-program" onclick="addAkun()">
                    <i class="bi bi-plus-circle" style="font-size: 50px;"></i>
                </div>
            </div>
        </div><!-- End Left side columns -->
        <!-- Right side columns -->

        </div>
        </div><!-- End News & Updates -->

        </div><!-- End Right side columns -->

        </div>
    </section>

</main><!-- End #main -->

<script>
    const deleteConfirm = (e) => {
        Swal.fire({
            title: 'Apakah anda yakin akan menghapus ini?',
            showCancelButton: true,
            confirmButtonText: 'Hapus',
            background: '#fff',
            color: '#000',
        }).then((result) => {
            if (result.isConfirmed) {
                deleteAkun(e)
            }
        })
    }

    const deleteAkun = (e) => {
        const id = e.id.split('-')[1]
        deleteData('api/akun/' + id, {
                token: "<?= $_COOKIE['token'] ?>"
            })
            .then((data) => {
                if(data.error) throw data
                e.parentNode.parentNode.parentNode.parentNode.classList.add('d-none')
                show(data.message)
            })
            .catch((err) => {
                show(err.error)
            })

    }

    const addAkun = () => {
        const rowContainer = document.querySelector('#row-container')
        const div = document.createElement('div')
        const colPlus = document.querySelector('#col-plus')
        div.innerHTML = `<div class="col-lg-6" id="">
                <div class="card info-card sales-card">
                    <div class="card-body">
                        <h5 class="mt-4 d-flex justify-content-between align-items-center">
                            <span>
                                <span class="card-title" id="heading-">Nama</span>
                                <div id="spinner-" class="d-none spinner-border spinner-border-sm text-success mx-3" role="status"></div>
                            </span>
                            <span id="" onclick="deleteConfirm(this)" class="float-right text-danger"><i class="bi bi-trash"></i></span>
                        </h5>
                        <div class="form-group mt-3">
                            <input type="text" class="form-control" id="nama-" aria-describedby="emailHelp" placeholder="Nama" value="" onblur="postAkun(this.parentNode.parentNode)">
                            <div class="invalid-feedback">
                                Nama harus diisi
                            </div>
                        </div>
                        <div class="form-group mt-3">
                            <input type="password" class="form-control" id="password" aria-describedby="emailHelp" placeholder="New Password" value="" onblur="postAkun(this.parentNode.parentNode)">
                            <div class="invalid-feedback">
                                Password harus diisi
                            </div>
                        </div>
                        <div class="form-group mt-3">
                            <input type="password" class="form-control" id="password-confirm" aria-describedby="emailHelp" placeholder="Password Confirm" value="" onblur="postAkun(this.parentNode.parentNode)">
                            <div class="invalid-feedback">
                                Konfirmasi harus sesuai
                            </div>
                        </div>
                    </div>
                </div>
            </div>`.trim()
        rowContainer.insertBefore(div.firstChild, colPlus)
    }


    const postAkun = async (e) => {
        const nama = e.childNodes[3].childNodes[1]
        const password = e.childNodes[5].childNodes[1]
        const passwordConfirm = e.childNodes[7].childNodes[1]
        const trash = e.childNodes[1].childNodes[3]
        const spinner = e.childNodes[1].childNodes[1].childNodes[3]
        const heading = e.childNodes[1].childNodes[1].childNodes[1]

        if (nama.value == "") nama.classList.add('is-invalid')
        else nama.classList.remove('is-invalid')
        if (password.value == "") password.classList.add('is-invalid')
        else password.classList.remove('is-invalid')
        if (passwordConfirm.value !== password.value) passwordConfirm.classList.add('is-invalid')
        else passwordConfirm.classList.remove('is-invalid')


        if (nama.value == "" || password.value == "" || passwordConfirm.value !== password.value) return false


        postData('/api/akun', {
                nama: nama.value,
                password: password.value,
                token: "<?= $_COOKIE['token'] ?>"
            }).then((data) => {
                console.log(data); // JSON data parsed by `data.json()` call
                id = data.id
                heading.id = 'heading-' + id
                heading.textContent = nama.value
                spinner.id = 'spinner-' + id
                nama.id = 'nama-' + id
                nama.setAttribute('onblur', 'putAkun(this.parentNode.parentNode)')
                password.id = 'password-' + id
                password.setAttribute('onblur', 'putAkun(this.parentNode.parentNode)')
                passwordConfirm.id = 'password-confirm-' + id
                passwordConfirm.setAttribute('onblur', 'putAkun(this.parentNode.parentNode)')
                trash.id = 'trash-' + id
                trash.setAttribute('onclick', 'deleteConfirm(this.parentNode.parentNode)')
                show('Data berhasil ditambahkan')
                addInputPasswordLama(e, id)
                password.value = ''
                passwordConfirm.value = ''
            })
            .catch((err) => {
                console.log(err);
            })
    }

    const putAkun = async (e) => {
        const nama = e.childNodes[3].childNodes[1]
        const password = e.childNodes[5].childNodes[1]
        const passwordConfirm = e.childNodes[7].childNodes[1]
        const passwordLama = e.childNodes[9].childNodes[1]
        const trash = e.childNodes[1].childNodes[3]
        const spinner = e.childNodes[1].childNodes[1].childNodes[3]
        const heading = e.childNodes[1].childNodes[1].childNodes[1]
        let id = e.childNodes[1].childNodes[1].childNodes[1].id.split('-')[1]

        if (nama.value == "") nama.classList.add('is-invalid')
        else nama.classList.remove('is-invalid')
        if (password.value == "") password.classList.add('is-invalid')
        else password.classList.remove('is-invalid')
        if (passwordLama.value == "") passwordLama.classList.add('is-invalid')
        else passwordLama.classList.remove('is-invalid')
        if (passwordConfirm.value !== password.value) passwordConfirm.classList.add('is-invalid')
        else passwordConfirm.classList.remove('is-invalid')


        if (nama.value == "" || password.value == "" || passwordConfirm.value !== password.value || passwordLama.value == "") return false


        putData('/api/akun/' + id, {
                nama: nama.value,
                password: password.value,
                passwordLama: passwordLama.value,
                token: "<?= $_COOKIE['token'] ?>"
            }).then((data) => {
                console.log(data); // JSON data parsed by `data.json()` call
                id = data.id
                heading.id = 'heading-' + id
                heading.textContent = nama.value
                spinner.id = 'spinner-' + id
                show('Data berhasil ditambahkan')
                addInputPasswordLama(e, id)
                password.value = ''
                passwordConfirm.value = ''
                passwordLama.value = ''
            })
            .catch((err) => {
                console.log(err);
            })
    }

    const addInputPasswordLama = (e, id) => {
        const div = document.createElement('div')
        div.classList.add('form-group')
        div.classList.add('mt-3')
        div.innerHTML = `<input type="password" class="form-control" aria-describedby="emailHelp" placeholder="Password Lama" value="" onblur="putAkun(this.parentNode.parentNode)">
                            <div class="invalid-feedback">
                                Konfirmasi harus sesuai
                            </div>`.trim()
        e.appendChild(div)
        div.id = 'password-lama-' + id
    }
</script>
@endsection
