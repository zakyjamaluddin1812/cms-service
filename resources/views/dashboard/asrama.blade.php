@extends('../template')
@section('title', 'Asrama')
@section('main')
<main id="main" class="main">

    <div class="pagetitle">
        <h1>Asrama</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                <li class="breadcrumb-item active">Asrama</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="section dashboard">
        <div class="row" id="row-container">
            @foreach($asrama as $as)
            <div class="col-lg-6" id="">
                <div class="card info-card sales-card">
                    <div class="card-body">
                        <h5 class="mt-4 d-flex justify-content-between align-items-center">
                            <span>
                                <span class="card-title" id="heading-{{$as->id}}">{{$as->title}}</span>
                                <div id="spinner-{{$as->id}}" class="d-none spinner-border spinner-border-sm text-success mx-3" role="status"></div>
                            </span>
                            <span id="trash-{{$as->id}}" onclick="deleteConfirm(this)" class="float-right text-danger"><i class="bi bi-trash"></i></span>
                        </h5>
                        <div class="position-relative img-banner text-center">
                            <span id="icon-file-{{$as->id}}" onclick="openFile(this)" class="shadow position-absolute top-50 start-50 translate-middle badge rounded-circle bg-light p-2">
                                <i class="ri-camera-fill"></i>

                            </span>
                            <img class="my-3" width="100%" id="image-{{$as->id}}" src="assets/img/asrama/{{$as->icon}}" alt="">
                        </div>

                        <div class="form-group mt-3">
                            <input type="text" class="form-control" id="title-{{$as->id}}" aria-describedby="emailHelp" placeholder="Title" value="{{$as->title}}" onblur="editTitle(this)">
                        </div>
                        <div class="form-group mt-3">
                            <textarea class="form-control" id="content-{{$as->id}}" placeholder="Deskripsi" rows="7" onblur="editSubtitle(this)">{{$as->subtitle}}</textarea>
                            <div class="invalid-feedback">
                                Konten harus diisi
                            </div>
                        </div>
                        <div class="form-group mt-3">
                            <input type="text" class="form-control" id="link-{{$as->id}}" aria-describedby="emailHelp" placeholder="Tautan" value="{{$as->link}}" onblur="editLink(this)">
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            <div id="col-plus" style="min-height: 380px;" class="col-lg-6 d-flex justify-content-center align-items-center">
                <div class="card d-flex justify-content-center align-items-center rounded-circle add-program" onclick="addAsrama()">
                    <i class="bi bi-plus-circle" style="font-size: 50px;"></i>
                </div>
            </div>
        </div><!-- End Left side columns -->
        <!-- Right side columns -->

        </div>
        </div><!-- End News & Updates -->

        </div><!-- End Right side columns -->

        </div>
    </section>

</main><!-- End #main -->

<script>
    const editTitle = (e) => {
        const id = e.id.split('-')[1]
        const spinner = document.getElementById('spinner-' + id)
        const heading = document.getElementById('heading-' + id)
        spinner.classList.remove('d-none')

        console.log(e.value);
        putData('api/asrama/title/' + id, {
                data: e.value,
                token: `<?= $_COOKIE['token'] ?>`
            })
            .then((data) => {
                if (data.error) throw data
                show(data.message)
                spinner.classList.add('d-none')
                heading.textContent = e.value
            })
            .catch((err) => {
                show(err.error)

                console.log(err);
                spinner.classList.add('d-none')
            })

    }
    const openFile = (e) => {
        const id = e.id.split('-')[2]
        const inputFile = document.createElement('input')
        const spinner = document.querySelector('#spinner-' + id)
        spinner.classList.remove('d-none')
        const image = document.querySelector('#image-' + id)
        inputFile.type = 'file'
        inputFile.onchange = (e) => {
            putImage('api/asrama/image/' + id, {
                    data: inputFile.files[0],
                    token: `<?= $_COOKIE['token'] ?>`
                })
                .then((data) => {
                    if (data.error) throw data
                    show(data.message)
                    spinner.classList.add('d-none')
                    image.src = URL.createObjectURL(inputFile.files[0])
                })
                .catch((err) => {
                    show(err.error)

                    console.log(err);
                    spinner.classList.add('d-none')
                })
        }
        inputFile.click()
    }

    const editSubtitle = (e) => {
        const id = e.id.split('-')[1]
        const spinner = document.getElementById('spinner-' + id)
        spinner.classList.remove('d-none')
        console.log(e.value);
        putData('api/asrama/subtitle/' + id, {
                data: e.value,
                token: `<?= $_COOKIE['token'] ?>`
            })
            .then((data) => {
                if (data.error) throw data
                show(data.message)
                spinner.classList.add('d-none')
            })
            .catch((err) => {
                show(err.error)

                console.log(err);
                spinner.classList.add('d-none')
            })
    }

    const editLink = (e) => {
        const id = e.id.split('-')[1]
        const spinner = document.getElementById('spinner-' + id)
        spinner.classList.remove('d-none')
        console.log(e.value);
        putData('api/asrama/link/' + id, {
                data: e.value,
                token: `<?= $_COOKIE['token'] ?>`
            })
            .then((data) => {
                if (data.error) throw data
                show(data.message)
                spinner.classList.add('d-none')
            })
            .catch((err) => {
                show(err.error)

                console.log(err);
                spinner.classList.add('d-none')
            })
    }

    const deleteConfirm = (e) => {
        Swal.fire({
            title: 'Apakah anda yakin akan menghapus ini?',
            showCancelButton: true,
            confirmButtonText: 'Hapus',
            background: '#fff',
            color: '#000',
        }).then((result) => {
            if (result.isConfirmed) {
                deleteAsrama(e)
            }
        })
    }

    const deleteAsrama = (e) => {
        const id = e.id.split('-')[1]
        deleteData('api/asrama/' + id, {
                token: `<?= $_COOKIE['token'] ?>`
            })
            .then((data) => {
                if (data.error) throw data
                show(data.message)
                e.parentNode.parentNode.parentNode.parentNode.classList.add('d-none')
            })
            .catch((err) => {
                show(err.error)

                console.log(err);
            })

    }

    const addAsrama = () => {
        const rowContainer = document.querySelector('#row-container')
        const div = document.createElement('div')
        const colPlus = document.querySelector('#col-plus')
        div.innerHTML = `<div class="col-lg-6" id="">
                <div class="card info-card sales-card">
                    <div class="card-body">
                        <h5 class="mt-4 d-flex justify-content-between align-items-center">
                            <span>
                                <span class="card-title" id="heading-">Judul</span>
                                <div id="spinner-" class="d-none spinner-border spinner-border-sm text-success mx-3" role="status"></div>
                            </span>
                            <span id="" onclick="deleteConfirm(this)" class="float-right text-danger"><i class="bi bi-trash"></i></span>
                        </h5>
                        <div class="position-relative img-banner text-center">
                            <span id="icon-file-" onclick="prepareFile(this.parentNode.parentNode)" class="shadow position-absolute top-50 start-50 translate-middle badge rounded-circle bg-light p-2">
                                <i class="ri-camera-fill"></i>

                            </span>
                            <img width="100%" id="image-" src="assets/img/default.jpg" alt="">
                        </div>
                        <p class="text-danger d-none">Silahkan pilih gambar</p>


                        <div class="form-group mt-3">
                            <input type="text" class="form-control" id="title-" aria-describedby="emailHelp" placeholder="Title" value="" onblur="postAsrama(this.parentNode.parentNode)">
                            <div class="invalid-feedback">
                                Judul harus diisi
                            </div>
                        </div>
                        <div class="form-group mt-3">
                            <textarea class="form-control" id="content-" placeholder="Deskripsi" rows="7" onblur="postAsrama(this.parentNode.parentNode)"></textarea>
                            <div class="invalid-feedback">
                                Deskripsi harus diisi
                            </div>
                        </div>
                        <div class="form-group mt-3">
                            <input type="text" class="form-control" id="link" aria-describedby="emailHelp" placeholder="Tautan" value="" onblur="postAsrama(this.parentNode.parentNode)">
                            <div class="invalid-feedback">
                                Tautan harus diisi
                            </div>
                        </div>
                    </div>
                </div>
            </div>`.trim()
        rowContainer.insertBefore(div.firstChild, colPlus)
    }

    const prepareFile = (e) => {
        const image = e.childNodes[3].childNodes[3]
        const inputFile = document.createElement('input')
        const spinner = e.childNodes[1].childNodes[1].childNodes[3]
        spinner.classList.remove('d-none')
        inputFile.type = 'file'
        inputFile.onchange = (el) => {
            image.src = URL.createObjectURL(inputFile.files[0])
            spinner.classList.add('d-none')
            postAsrama(e)
        }
        inputFile.click()

    }

    const postAsrama = async (e) => {

        const imageButton = e.childNodes[3].childNodes[1]
        const imageError = e.childNodes[5]
        const image = e.childNodes[3].childNodes[3]
        const title = e.childNodes[7].childNodes[1]
        const subtitle = e.childNodes[9].childNodes[1]
        const trash = e.childNodes[1].childNodes[3]
        const spinner = e.childNodes[1].childNodes[1].childNodes[3]
        const heading = e.childNodes[1].childNodes[1].childNodes[1]
        const imageName = image.src.split('/')[image.src.split('/').length - 1]
        const imageFile = await fileFromUrl(image)
        const link = e.childNodes[11].childNodes[1]



        if (imageName == 'default.jpg') imageError.classList.remove('d-none')
        else imageError.classList.add('d-none')
        if (title.value == "") title.classList.add('is-invalid')
        else title.classList.remove('is-invalid')
        if (subtitle.value == "") subtitle.classList.add('is-invalid')
        else subtitle.classList.remove('is-invalid')
        if (link.value == "") link.classList.add('is-invalid')
        else link.classList.remove('is-invalid')


        if (title.value == "" || subtitle.value == "" || imageName == "default.jpg" || link.value == "") return false


        postMultipart('/api/asrama', {
                image: imageFile,
                title: title.value,
                content: [
                    subtitle.value,
                    link.value
                ],
                token: `<?= $_COOKIE['token'] ?>`
            }).then((data) => {
                if (data.error) throw data
                show(data.message)
                id = data.id
                heading.id = 'heading-' + id
                heading.textContent = title.value
                spinner.id = 'spinner-' + id
                title.id = 'title-' + id
                title.setAttribute('onblur', 'editTitle(this)')
                subtitle.id = 'subtitle-' + id
                subtitle.setAttribute('onblur', 'editSubtitle(this)')
                link.id = 'link-' + id
                link.setAttribute('onblur', 'editLink(this)')
                trash.id = 'trash-' + id
                trash.setAttribute('onclick', 'deleteConfirm(this)')
            })
            .catch((err) => {
                show(err.error)

            })
    }

    const fileFromUrl = async (image) => {
        const imageName = image.src.split('/')[image.src.split('/').length - 1]
        const data = await fetch(image.src).then(r => r.blob());
        let metadata = {
            type: 'image/jpeg'
        };
        let file = new File([data], imageName, metadata);
        return file
    }
</script>
@endsection
