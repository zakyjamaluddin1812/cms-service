@extends('../template')
@section('title', 'Banner')
@section('main')
<main id="main" class="main">

    <div class="pagetitle">
        <h1>Banner</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                <li class="breadcrumb-item active">Banner</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="section dashboard">
        <div class="row">

            <!-- Left side columns -->
            <div class="col-lg-6">
                <div class="card info-card sales-card">

                    <div class="card-body">
                        <h5 class="card-title">Banner Image
                            <div id="banner-image-spinner" class="d-none spinner-border spinner-border-sm text-success mx-3" role="status">
                                <span class="visually-hidden">Loading...</span>
                            </div>
                        </h5>

                        <div class="position-relative img-banner">
                            <span onclick="openFile()" class="shadow position-absolute top-50 start-50 translate-middle badge rounded-circle bg-light p-2">
                                <i class="ri-camera-fill"></i>

                            </span>
                            <img width="100%" id="banner-image" src="{{asset('assets/img/banner/'.$banner->image)}}" alt="">
                        </div>
                    </div>

                </div>
            </div><!-- End Left side columns -->

            <!-- Right side columns -->
            <div class="col-lg-6">

                <!-- Recent Activity -->
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Banner Text
                            <div id="banner-text-spinner" class="d-none spinner-border spinner-border-sm text-success mx-3" role="status">
                                <span class="visually-hidden">Loading...</span>
                            </div>
                        </h5>
                        <div class="form-group mb-3">
                            <input type="text" class="form-control" id="banner-title" aria-describedby="emailHelp" placeholder="Judul" value="{{ $banner['title'] }}">
                        </div>
                        <div class="form-group mb-3">
                            <textarea class="form-control" id="banner-subtitle" placeholder="Deskripsi" rows="3">{{ $banner['subtitle'] }}</textarea>
                        </div>
                        <!-- <form action="/api/banner/image" method="post" enctype="multipart/form-data">
                            @csrf
                            <input type="file" name="data" id="">
                            <button type="submit">kirim</button>
                        </form> -->
                    </div>
                </div><!-- End Recent Activity -->

            </div><!-- End sidebar recent posts-->

        </div>
        </div><!-- End News & Updates -->

        </div><!-- End Right side columns -->

        </div>
    </section>

</main><!-- End #main -->
<form class="d-none" action="/api/banner/image" method="post" id="formFile" enctype="multipart/form-data">
    <input type="file" id="inputFile" name="data">
    <button type="submit">Kirim</button>
</form>

<script>
    const title = document.getElementById('banner-title')
    const subtitle = document.getElementById('banner-subtitle')
    const bannerTextSpinner = document.getElementById('banner-text-spinner')
    const bannerImageSpinner = document.getElementById('banner-image-spinner')
    const bannerImage = document.getElementById('banner-image')
    const inputFile = document.querySelector('#inputFile')
    const form = document.getElementById('formFile')



    title.addEventListener('blur', (e) => {
        bannerTextSpinner.classList.remove('d-none')
        putData('api/banner/title/1', {
                data: e.target.value,
                token: "<?= $_COOKIE['token'] ?>"
            })
            .then((data) => {
                if (data.error) throw data
                bannerTextSpinner.classList.add('d-none')
                show(data.message)
            })
            .catch((err) => {
                bannerTextSpinner.classList.add('d-none')
                show(err.error)
            })
    })

    subtitle.addEventListener('blur', (e) => {
        console.log('debug');
        bannerTextSpinner.classList.remove('d-none')
        putData('api/banner/desc/1', {
                data: e.target.value,
                token: "<?= $_COOKIE['token'] ?>"
            })
            .then((data) => {
                if(data.error) throw data
                bannerTextSpinner.classList.add('d-none')
                show(data.message)
            })
            .catch((err) => {
                show(err.error)
                bannerTextSpinner.classList.add('d-none')
            })
    })


    const openFile = () => {
        inputFile.onchange = e => {
            putImage('api/banner/image', {
                    data: inputFile.files[0],
                    token: "<?= $_COOKIE['token'] ?>"
                })
                .then((data) => {
                    if(data.error) throw data
                    show(data.message)
                    bannerTextSpinner.classList.add('d-none')
                    bannerImage.src = URL.createObjectURL(inputFile.files[0])
                })
                .catch((err) => {
                    bannerTextSpinner.classList.add('d-none')
                    show(err.error)
                })
        }
        inputFile.click();
    }
</script>

@endsection
