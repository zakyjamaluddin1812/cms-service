@extends('../template')
@section('title', 'Blog')
@section('main')
<style>
    #prev img {
        width: 100%;
    }
</style>
<main id="main" class="main">

    <div class="pagetitle">
        <h1>Blog</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                <li class="breadcrumb-item active">Blog</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="section dashboard">
        <div class="card info-card sales-card">
            <div class="card-body">
                <form action="">
                    <h5 class="mt-4 d-flex justify-content-between align-items-center">
                        <span>
                            <span class="card-title" id="heading">Judul blog</span>
                            <div id="spinner" class="d-none spinner-border spinner-border-sm text-success mx-3" role="status"></div>
                        </span>
                        <span id="save" onclick="saveConfirm(this)" class="float-right text-success"><i class="bi bi-folder-check"></i></span>
                    </h5>
                    <div class="row">
                        <div class="col-lg-6 mb-3" id="">
                            <div class="position-relative img-banner">
                                <span id="icon-file" onclick="openFile(this)" class="shadow position-absolute top-50 start-50 translate-middle badge rounded-circle bg-light p-2">
                                    <i class="ri-camera-fill"></i>

                                </span>
                                <img width="100%" id="image" src="{{ asset('assets/img/default.jpg')}}" alt="">
                            </div>
                            <p id="image-error" class="text-danger d-none">Silahkan pilih gambar thumbnail</p>


                        </div>
                        <div class="col-lg-6" id="row-container">
                            <div class="form-group">
                                <input type="text" class="form-control" id="title" aria-describedby="emailHelp" placeholder="Title" value="">
                                <div class="invalid-feedback">
                                    Judul harus diisi
                                </div>
                            </div>
                            <div class="form-group mt-3">
                                <textarea class="form-control" id="content" placeholder="Deskripsi" rows="7"></textarea>
                                <div class="invalid-feedback">
                                    Deskripsi harus diisi
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-lg-6">
                            <div class="input-group">
                                <textarea placeholder="Markdown" id="mark" rows="40" class="form-control" aria-label="With textarea"></textarea>
                                <div class="invalid-feedback">
                                    Konten harus diisi
                                </div>
                            </div>
                            <p onclick="openFileMd()"><i class="bi bi-file-earmark-image "></i> Klik untuk memasukkan gambar</p>
                        </div>
                        <hr class="my-5 d-lg-none">

                        <div class="col-lg-6">
                            <p class="text-center">Preview</p>
                            <div id="prev">

                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- End Left side columns -->
        <!-- Right side columns -->
    </section>

</main><!-- End #main -->

<script>
    const openFile = (e) => {
        const id = e.id.split('-')[2]
        const inputFile = document.createElement('input')
        const spinner = document.querySelector('#spinner')
        spinner.classList.remove('d-none')
        const image = document.querySelector('#image')
        inputFile.type = 'file'
        inputFile.onchange = (e) => {
            spinner.classList.add('d-none')
            image.src = URL.createObjectURL(inputFile.files[0])
        }
        inputFile.click()
    }

    const saveConfirm = (e) => {
        Swal.fire({
            title: 'Apakah anda yakin akan menyimpan blog ini?',
            showCancelButton: true,
            confirmButtonText: 'Simpan',
            background: '#fff',
            color: '#000',
        }).then((result) => {
            if (result.isConfirmed) {
                postBlog()
            }
        })
    }

    const postBlog = async () => {
        const image = document.querySelector('#image')
        const imageError = document.querySelector('#image-error')
        const title = document.querySelector('#title')
        const content = document.querySelector('#content')
        const imageFile = await fileFromUrl(image)
        const mark = document.querySelector('#mark')
        // console.log(contents); return false

        if (image.src.split('/')[image.src.split('/').length - 1] == 'default.jpg') imageError.classList.remove('d-none')
        else imageError.classList.add('d-none')
        if (title.value == '') title.classList.add('is-invalid')
        else title.classList.remove('is-invalid')
        if (content.value == '') content.classList.add('is-invalid')
        else content.classList.remove('is-invalid')
        if (mark.value == '') mark.classList.add('is-invalid')
        else mark.classList.remove('is-invalid')

        if (image.src.split('/')[image.src.split('/').length - 1] == 'default.jpg' || title.value == '' || content.value == '') return false

        postMultipart('<?= url('api/blog/') ?>', {
                image: imageFile,
                title: title.value,
                deskripsi: content.value,
                content : mark.value,
                token: "<?= $_COOKIE['token'] ?>"
            })
            .then((data) => {
                if (data.error) throw data
                console.log(data.message);
                show(data.message)
                window.location.href = '<?= url('blog/') ?>'
            })
            .catch((err) => {
                show(err.error)
            })


    }

    const fileFromUrl = async (image) => {
        const imageName = image.src.split('/')[image.src.split('/').length - 1]
        const data = await fetch(image.src).then(r => r.blob());
        let metadata = {
            type: 'image/jpeg'
        };
        let file = new File([data], imageName, metadata);
        return file
    }

    const mark = document.querySelector('#mark')
    const prev = document.querySelector('#prev')

    mark.addEventListener('keyup', (e) => {
        prev.innerHTML = marked.parse(e.target.value)
    })

    const openFileMd = () => {
        const inputFile = document.createElement('input')
        inputFile.type = 'file'
        inputFile.onchange = (e) => {
            putImage(' <?= url('api/blog/image/') ?>', {
                    data: inputFile.files[0]
                })
                .then((data) => {
                    if (data.error) throw data
                    show(data.message)
                    mark.value += `![Ini Gambar](${window.location.origin + '/' + data.path} "Ini adalah Gambar")`
                })
                .catch((err) => {
                    console.log(err);
                })
        }
        inputFile.click()
    }
</script>
@endsection
