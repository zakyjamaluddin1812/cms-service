@extends('../template')
@section('title', 'Blog')
@section('main')
<style>
    #prev img{
        width: 100%;
    }
</style>
<main id="main" class="main">

    <div class="pagetitle">
        <h1>Blog</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                <li class="breadcrumb-item active">Blog</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="section dashboard">
        <div class="card info-card sales-card">
            <div class="card-body">
                <h5 class="mt-4 d-flex justify-content-between align-items-center">
                    <span>
                        <span class="card-title" id="heading-{{$blog->id}}">{{$blog->title}}</span>
                        <div id="spinner" class="d-none spinner-border spinner-border-sm text-success mx-3" role="status"></div>
                    </span>
                    <span id="trash-{{$blog->id}}" onclick="deleteConfirm(this)" class="float-right text-danger"><i class="bi bi-trash"></i></span>
                </h5>
                <div class="row">
                    <div class="col-lg-6 mb-3" id="">
                        <div class="position-relative img-banner">
                            <span id="icon-file-{{$blog->id}}" onclick="openFile(this)" class="shadow position-absolute top-50 start-50 translate-middle badge rounded-circle bg-light p-2">
                                <i class="ri-camera-fill"></i>

                            </span>
                            <img width="100%" id="image" src="{{ asset('assets/img/blog/' . $blog->thumbnail)}}" alt="">
                        </div>


                    </div>
                    <div class="col-lg-6" id="row-container">
                        <div class="form-group">
                            <input type="text" class="form-control" id="title-{{$blog->id}}" aria-describedby="emailHelp" placeholder="Title" value="{{$blog->title}}" onblur="editTitle(this)">
                        </div>
                        <div class="form-group mt-3">
                            <textarea class="form-control" id="content-{{$blog->id}}" placeholder="Paragraf" rows="7" onblur="editDeskripsi(this)">{{$blog->deskripsi}}</textarea>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                        <div class="col-lg-6">
                            <div class="input-group">
                                <textarea onblur="editContent(this)" placeholder="Markdown" id="mark" rows="40" class="form-control" aria-label="With textarea">{{$blog->content}}</textarea>
                                <div class="invalid-feedback">
                                    Konten harus diisi
                                </div>
                            </div>
                            <p onclick="openFileMd()"><i class="bi bi-file-earmark-image "></i> Klik untuk memasukkan gambar</p>
                        </div>
                        <hr class="my-5 d-lg-none">

                        <div class="col-lg-6">
                            <p class="text-center">Preview</p>
                            <div id="prev">
                            {!!str($blog->content)->markdown()!!}
                            </div>
                        </div>
                    </div>
            </div>
        </div><!-- End Left side columns -->
        <!-- Right side columns -->
    </section>

</main><!-- End #main -->

<script>
    const id = '{{$blog->id}}'
    const editTitle = (e) => {
        const spinner = document.getElementById('spinner')
        const heading = document.getElementById('heading-' + id)
        spinner.classList.remove('d-none')
        console.log(e.value)
        putData('<?= url('api/blog/title') ?>' + '/' + id, {
                data: e.value,
                token: "<?= $_COOKIE['token'] ?>"
            })
            .then((data) => {
                if(data.error) throw data
                show(data.message)
                spinner.classList.add('d-none')
                heading.textContent = e.value
            })
            .catch((err) => {
                console.log(err);
                show(err.error)
                spinner.classList.add('d-none')
            })

    }
    const openFile = (e) => {
        const inputFile = document.createElement('input')
        const spinner = document.querySelector('#spinner')
        spinner.classList.remove('d-none')
        const image = document.querySelector('#image')
        inputFile.type = 'file'
        inputFile.onchange = (e) => {
            putImage('<?= url('api/blog/image') ?>' + '/' + id, {
                    data: inputFile.files[0],
                    token: "<?= $_COOKIE['token'] ?>"
                })
                .then((data) => {
                    if(data.error) throw data
                show(data.message)
                    spinner.classList.add('d-none')
                    image.src = URL.createObjectURL(inputFile.files[0])
                })
                .catch((err) => {
                    console.log(err);
                show(err.error)
                    spinner.classList.add('d-none')
                })
        }
        inputFile.click()
    }

    const editContent = (e) => {
        const spinner = document.getElementById('spinner')
        spinner.classList.remove('d-none')
        console.log(e.value);
        if (e.value == '' || e.value == null) return deleteContent(e)
        putData('<?= url('api/blog/content') ?>' + '/' + id, {
                data: e.value,
                token: "<?= $_COOKIE['token'] ?>"
            })
            .then((data) => {
                if(data.error) throw data
                show(data.message)
                spinner.classList.add('d-none')
            })
            .catch((err) => {
                console.log(err);
                show(err.error)

                spinner.classList.add('d-none')
            })
    }

    const editDeskripsi = (e) => {
        const spinner = document.getElementById('spinner')
        spinner.classList.remove('d-none')
        putData('<?= url('api/blog/deskripsi') ?>' + '/' + id, {
                data: e.value,
                token: "<?= $_COOKIE['token'] ?>"
            })
            .then((data) => {
                if(data.error) throw data
                show(data.message)
                spinner.classList.add('d-none')
            })
            .catch((err) => {
                console.log(err);
                show(err.error)
                spinner.classList.add('d-none')
            })
    }

    const deleteContent = (e) => {
        const spinner = document.getElementById('spinner')
        spinner.classList.remove('d-none')
        deleteData('<?= url('api/blog/content') ?>' + '/' + id, {
                token: "<?= $_COOKIE['token'] ?>"
            })
            .then((data) => {
                if(data.error) throw data
                show(data.message)
                spinner.classList.add('d-none')
                e.classList.add('d-none')
            })
            .catch((err) => {
                console.log(err);
                show(err.error)

                spinner.classList.add('d-none')
            })
    }
    const deleteConfirm = (e) => {
        Swal.fire({
            title: 'Apakah anda yakin akan menghapus ini?',
            showCancelButton: true,
            confirmButtonText: 'Hapus',
            background: '#fff',
            color: '#000',
        }).then((result) => {
            if (result.isConfirmed) {
                deleteBlog(e)
            }
        })
    }


    const deleteBlog = (e) => {
        const spinner = document.getElementById('spinner')
        spinner.classList.remove('d-none')
        deleteData('<?= url('api/blog') ?>' + '/' + id, {
                token: "<?= $_COOKIE['token'] ?>"
            })
            .then((data) => {
                window.location.href = '<?= url('blog') ?>'
            })
            .catch((err) => {
                show(err.error)

                console.log(err);
                spinner.classList.add('d-none')
            })
    }
    const mark = document.querySelector('#mark')
    const prev = document.querySelector('#prev')

    mark.addEventListener('keyup', (e) => {
        prev.innerHTML = marked.parse(e.target.value)
    })

    const openFileMd = () => {
        const inputFile = document.createElement('input')
        inputFile.type = 'file'
        inputFile.onchange = (e) => {
            putImage(' <?= url('api/blog/image/') ?>', {
                    data: inputFile.files[0]
                })
                .then((data) => {
                    if (data.error) throw data
                    show(data.message)
                    mark.value += `\r\n![Ini Gambar](${window.location.origin + '/' + data.path} "Ini adalah Gambar")`
                    prev.innerHTML = marked.parse(mark.value)
                })
                .catch((err) => {
                    console.log(err);
                })
        }
        inputFile.click()
    }
</script>
@endsection
