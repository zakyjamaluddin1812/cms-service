@extends('../template')
@section('title', 'Blog')
@section('main')
<main id="main" class="main">

    <div class="pagetitle">
        <h1>Blog</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                <li class="breadcrumb-item active">Blog</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="section dashboard">
        <div class="row" id="row-container">
            @foreach($blog as $b)
            <div class="col-lg-4 col-6" onclick="detail(this)" id="blog-{{$b->id}}">
                <div class="card info-card sales-card">
                    <div class="card-body py-0">
                        <img class="my-4" width="100%" id="image-" src="{{ asset('assets/img/blog/' . $b->thumbnail)}}" alt="">
                        <h5>{{$b->title}}</h5>
                        <p>{{$b->deskripsi}}</p>
                    </div>
                </div>
            </div>
            @endforeach
            <div id="col-plus" class="col-lg-4 col-6 d-flex justify-content-center align-items-center">
                <div class="card d-flex justify-content-center align-items-center rounded-circle add-program" onclick="addBlog()">
                    <i class="bi bi-plus-circle" style="font-size: 50px;"></i>
                </div>
            </div>
        </div>
    </section>
</main><!-- End #main -->

<script>
    const detail = (e) => {
        const id = e.id.split('-')[e.id.split('-').length - 1]
        window.location.href = 'blog/' + id
    }

    const addBlog = () => {
        window.location.href = 'blog/add'
    }
</script>
@endsection
