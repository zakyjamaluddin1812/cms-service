@extends('../template')
@section('title', 'Kesiswaan')
@section('main')
<main id="main" class="main">

    <div class="pagetitle">
        <h1>Kesiswaan</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                <li class="breadcrumb-item active">Kesiswaan</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="section dashboard">
        <div class="row" id="row-container">
            @foreach($kesiswaan as $siswa)
            <div class="col-lg-6" id="">
                <div class="card info-card sales-card">
                    <div class="card-body">
                        <h5 class="mt-4 d-flex justify-content-between align-items-center">
                            <span>
                                <span class="card-title" id="heading-{{$siswa->id}}">{{$siswa->title}}</span>
                                <div id="spinner-{{$siswa->id}}" class="d-none spinner-border spinner-border-sm text-success mx-3" role="status"></div>
                            </span>
                            <span id="trash-{{$siswa->id}}" onclick="deleteConfirm(this)" class="float-right text-danger"><i class="bi bi-trash"></i></span>
                        </h5>
                        <div class="position-relative img-banner">
                            <span id="icon-file-{{$siswa->id}}" onclick="openFile(this)" class="shadow position-absolute top-50 start-50 translate-middle badge rounded-circle bg-light p-2">
                                <i class="ri-camera-fill"></i>

                            </span>
                            <img width="100%" id="image-{{$siswa->id}}" src="assets/img/kesiswaan/{{$siswa->image}}" alt="">
                        </div>

                        <div class="form-group mt-3">
                            <input type="text" class="form-control" id="title-{{$siswa->id}}" aria-describedby="emailHelp" placeholder="Title" value="{{$siswa->title}}" onblur="editTitle(this)">
                        </div>
                        <div class="form-group mt-3">
                            <textarea class="form-control" id="content-{{$siswa->id}}" placeholder="Deskripsi" rows="7" onblur="editContent(this)">{{$siswa->content}}</textarea>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            <div id="col-plus" style="min-height: 380px;" class="col-lg-6 d-flex justify-content-center align-items-center">
                <div class="card d-flex justify-content-center align-items-center rounded-circle add-program" onclick="addKesiswaan()">
                    <i class="bi bi-plus-circle" style="font-size: 50px;"></i>
                </div>
            </div>
        </div><!-- End Left side columns -->
        <!-- Right side columns -->

        </div>
        </div><!-- End News & Updates -->

        </div><!-- End Right side columns -->

        </div>
    </section>

</main><!-- End #main -->

<script>
    const editTitle = (e) => {
        const id = e.id.split('-')[1]
        const spinner = document.getElementById('spinner-' + id)
        const heading = document.getElementById('heading-' + id)
        spinner.classList.remove('d-none')
        console.log(e.value)
        putData('api/kesiswaan/title/' + id, {
                data: e.value,
                token: "<?= $_COOKIE['token'] ?>"
            })
            .then((data) => {
                if(data.error) throw data
                show(data.message)
                spinner.classList.add('d-none')
                heading.textContent = e.value
            })
            .catch((err) => {
                show(err.error)

                console.log(err);
                spinner.classList.add('d-none')
            })

    }
    const openFile = (e) => {
        const id = e.id.split('-')[2]
        const inputFile = document.createElement('input')
        const spinner = document.querySelector('#spinner-' + id)
        spinner.classList.remove('d-none')
        const image = document.querySelector('#image-' + id)
        inputFile.type = 'file'
        inputFile.onchange = (e) => {
            putImage('api/kesiswaan/image/' + id, {
                    data: inputFile.files[0],
                    token: "<?= $_COOKIE['token'] ?>"
                })
                .then((data) => {
                    if(data.error) throw data
                show(data.message)
                    image.src = URL.createObjectURL(inputFile.files[0])
                })
                .catch((err) => {
                show(err.error)

                    console.log(err);
                    spinner.classList.add('d-none')
                })
        }
        inputFile.click()
    }

    const editContent = (e) => {
        const id = e.id.split('-')[1]
        const spinner = document.getElementById('spinner-' + id)
        spinner.classList.remove('d-none')
        console.log(e.value);
        putData('api/kesiswaan/content/' + id, {
                data: e.value,
                token: "<?= $_COOKIE['token'] ?>"
            })
            .then((data) => {
                if(data.error) throw data
                show(data.message)
                spinner.classList.add('d-none')
            })
            .catch((err) => {
                show(err.error)

                console.log(err);
                spinner.classList.add('d-none')
            })
    }

    const deleteConfirm = (e) => {
        Swal.fire({
            title: 'Apakah anda yakin akan menghapus ini?',
            showCancelButton: true,
            confirmButtonText: 'Hapus',
            background: '#fff',
            color: '#000',
        }).then((result) => {
            if (result.isConfirmed) {
                deleteKesiswaan(e)
            }
        })
    }

    const deleteKesiswaan = (e) => {
        const id = e.id.split('-')[1]
        deleteData('api/kesiswaan/' + id, {
                token: "<?= $_COOKIE['token'] ?>"
            })
            .then((data) => {
                if(data.error) throw data
                show(data.message)
                e.parentNode.parentNode.parentNode.parentNode.classList.add('d-none')
            })
            .catch((err) => {
                show(err.error)

                console.log(err);
            })

    }

    const addKesiswaan = () => {
        const rowContainer = document.querySelector('#row-container')
        const div = document.createElement('div')
        const colPlus = document.querySelector('#col-plus')
        div.innerHTML = `<div class="col-lg-6" id="">
                <div class="card info-card sales-card">
                    <div class="card-body">
                        <h5 class="mt-4 d-flex justify-content-between align-items-center">
                            <span>
                                <span class="card-title" id="heading-">Judul</span>
                                <div id="spinner-" class="d-none spinner-border spinner-border-sm text-success mx-3" role="status"></div>
                            </span>
                            <span id="" onclick="deleteConfirm(this)" class="float-right text-danger"><i class="bi bi-trash"></i></span>
                        </h5>
                        <div class="position-relative img-banner">
                            <span id="icon-file-" onclick="prepareFile(this.parentNode.parentNode)" class="shadow position-absolute top-50 start-50 translate-middle badge rounded-circle bg-light p-2">
                                <i class="ri-camera-fill"></i>

                            </span>
                            <img width="100%" id="image-" src="assets/img/default.jpg" alt="">
                        </div>
                        <p class="text-danger d-none">Silahkan pilih gambar</p>


                        <div class="form-group mt-3">
                            <input type="text" class="form-control" id="title-" aria-describedby="emailHelp" placeholder="Title" value="" onblur="postKesiswaan(this.parentNode.parentNode)">
                            <div class="invalid-feedback">
                                Judul harus diisi
                            </div>
                        </div>
                        <div class="form-group mt-3">
                            <textarea class="form-control" id="content-" placeholder="Deskripsi" rows="7" onblur="postKesiswaan(this.parentNode.parentNode)"></textarea>
                            <div class="invalid-feedback">
                                Konten harus diisi
                            </div>
                        </div>
                    </div>
                </div>
            </div>`.trim()
        rowContainer.insertBefore(div.firstChild, colPlus)
    }

    const prepareFile = (e) => {
        console.log(e);

        const image = e.childNodes[3].childNodes[3]
        const inputFile = document.createElement('input')
        const spinner = e.childNodes[1].childNodes[1].childNodes[3]
        spinner.classList.remove('d-none')
        inputFile.type = 'file'
        inputFile.onchange = (el) => {
            image.src = URL.createObjectURL(inputFile.files[0])
            spinner.classList.add('d-none')
            console.log(inputFile.files[0]);
            postKesiswaan(e)
        }
        inputFile.click()

    }

    const postKesiswaan = async (e) => {
        console.log(e)

        const imageButton = e.childNodes[3].childNodes[1]
        const imageError = e.childNodes[5]
        const image = e.childNodes[3].childNodes[3]
        const title = e.childNodes[7].childNodes[1]
        const subtitle = e.childNodes[9].childNodes[1]
        const trash = e.childNodes[1].childNodes[3]
        const spinner = e.childNodes[1].childNodes[1].childNodes[3]
        const heading = e.childNodes[1].childNodes[1].childNodes[1]
        const imageName = image.src.split('/')[image.src.split('/').length - 1]
        const imageFile = await fileFromUrl(image)


        if (imageName == 'default.jpg') imageError.classList.remove('d-none')
        else imageError.classList.add('d-none')
        if (title.value == "") title.classList.add('is-invalid')
        else title.classList.remove('is-invalid')
        if (subtitle.value == "") subtitle.classList.add('is-invalid')
        else subtitle.classList.remove('is-invalid')


        if (title.value == "" || subtitle.value == "" || imageName == "default.jpg") return false


        postMultipart('/api/kesiswaan', {
                image: imageFile,
                title: title.value,
                content: subtitle.value,
                token: "<?= $_COOKIE['token'] ?>"
            }).then((data) => {
                if(data.error) throw data
                show(data.message)
                id = data.id
                heading.id = 'heading-' + id
                heading.textContent = title.value
                spinner.id = 'spinner-' + id
                title.id = 'title-' + id
                title.setAttribute('onblur', 'editTitle(this)')
                subtitle.id = 'subtitle-' + id
                subtitle.setAttribute('onblur', 'editContent(this)')
                trash.id = 'trash-' + id
                trash.setAttribute('onclick', 'deleteConfirm(this)')
            })
            .catch((err) => {
                show(err.error)

                console.log(err);
            })
    }

    const fileFromUrl = async (image) => {
        const imageName = image.src.split('/')[image.src.split('/').length - 1]
        const data = await fetch(image.src).then(r => r.blob());
        let metadata = {
            type: 'image/jpeg'
        };
        let file = new File([data], imageName, metadata);
        return file
    }
</script>
@endsection
