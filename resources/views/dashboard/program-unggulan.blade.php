@extends('../template')
@section('title', 'Program Unggulan')
@section('main')
<main id="main" class="main">

    <div class="pagetitle">
        <h1>Program Unggulan</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                <li class="breadcrumb-item active">Program Unggulan</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="section dashboard">
        <div class="row" id="row-container">

            <!-- Left side columns -->
            @foreach($programUnggulan as $program)
            <div class="col-lg-6" id="hahh">
                <div class="card info-card sales-card">
                    <div class="card-body">
                        <h5 class="mt-4 d-flex justify-content-between align-items-center">
                            <span>
                                <span class="card-title" id="heading-{{$program->id}}">{{$program->title}}</span>
                                <div id="spinner-{{ $program->id }}" class="d-none spinner-border spinner-border-sm text-success mx-3" role="status"></div>
                            </span>
                            <span id="trash-{{ $program->id }}" onclick="deleteConfirm(this)" class="float-right text-danger"><i class="bi bi-trash"></i></span>
                        </h5>
                        <div class="row">

                            <div class="col-9">
                                <div class="form-group mb-3">
                                    <input type="text" class="form-control" id="icon-{{$program->id}}" aria-describedby="emailHelp" placeholder="Icon" value="{{$program->icon}}" onblur="editIcon(this)">
                                </div>
                                <div class="form-group mb-3">
                                    <input type="text" class="form-control" id="title-{{$program->id}}" aria-describedby="emailHelp" placeholder="Nama Jurusan" value="{{$program->title}}" onblur="editTitle(this)">
                                </div>
                            </div>
                            <div class="col-3 d-flex justify-content-center align-items-center">
                                <i id="icon-prev-{{$program->id}}" class="{{$program->icon}}" style="font-size: 50px;"></i>
                            </div>
                        </div>
                        <div class="form-group mb-3">
                            <textarea class="form-control" id="subtitle-{{$program->id}}" placeholder="Deskripsi" rows="5" onblur="editSubtitle(this)">{{$program->subtitle}}</textarea>
                        </div>
                    </div>
                </div>
            </div><!-- End Left side columns -->
            @endforeach

            <div id="col-plus" style="min-height: 380px;" class="col-lg-6 d-flex justify-content-center align-items-center">
                <div class="card d-flex justify-content-center align-items-center rounded-circle add-program" onclick="addProgram()">
                    <i class="bi bi-plus-circle" style="font-size: 50px;"></i>
                </div>
            </div>

            <!-- Right side columns -->

        </div>
        </div><!-- End News & Updates -->

        </div><!-- End Right side columns -->

        </div>
    </section>

</main><!-- End #main -->
<script>
    const editIcon = (e) => {
        const id = e.id.split('-')[1]
        const spinner = document.getElementById('spinner-' + id)
        const icon = document.querySelector('#icon-prev-' + id)
        console.log(e.value)
        putData('api/program-unggulan/icon/' + id, {
                data: e.value,
                token: "<?= $_COOKIE['token'] ?>"
            })
            .then((data) => {
                if(data.error) throw data
                show(data.message)
                spinner.classList.add('d-none')
                icon.className = e.value
            })
            .catch((err) => {
                console.log(err);
                show(err.error)

                spinner.classList.add('d-none')
            })
    }
    const editTitle = (e) => {
        const id = e.id.split('-')[1]
        const spinner = document.getElementById('spinner-' + id)
        const heading = document.getElementById('heading-' + id)
        spinner.classList.remove('d-none')
        console.log(e.value);
        putData('api/program-unggulan/title/' + id, {
                data: e.value,
                token: "<?= $_COOKIE['token'] ?>"
            })
            .then((data) => {
                if(data.error) throw data
                show(data.message)
                spinner.classList.add('d-none')
                heading.textContent = e.value
            })
            .catch((err) => {
                console.log(err);
                show(err.error)

                spinner.classList.add('d-none')
            })
    }

    const editSubtitle = (e) => {
        const id = e.id.split('-')[1]
        const spinner = document.getElementById('spinner-' + id)
        spinner.classList.remove('d-none')
        console.log(e.value);
        putData('api/program-unggulan/subtitle/' + id, {
                data: e.value,
                token: "<?= $_COOKIE['token'] ?>"
            })
            .then((data) => {
                if(data.error) throw data
                show(data.message)
                spinner.classList.add('d-none')
            })
            .catch((err) => {
                console.log(err);
                spinner.classList.add('d-none')
                show(err.error)

            })
    }




    const deleteConfirm = (e) => {
        Swal.fire({
            title: 'Apakah anda yakin akan menghapus program ini?',
            showCancelButton: true,
            confirmButtonText: 'Hapus',
            background: '#fff',
            color: '#000',
        }).then((result) => {
            if (result.isConfirmed) {
                deleteProgram(e)
            }
        })
    }

    const deleteProgram = (e) => {
        const id = e.id.split('-')[1]
        deleteData('api/program-unggulan/' + id, {
                token: "<?= $_COOKIE['token'] ?>"
            })
            .then((data) => {
                if(data.error) throw data
                show(data.message)
                e.parentNode.parentNode.parentNode.parentNode.classList.add('d-none')
            })
            .catch((err) => {
                console.log(err);
                show(err.error)

            })

    }

    const addProgram = () => {
        const rowContainer = document.querySelector('#row-container')
        const div = document.createElement('div')
        const colPlus = document.querySelector('#col-plus')
        div.innerHTML = `
            <div class="col-lg-6">
                <div class="card info-card sales-card">
                    <div class="card-body">
                        <h5 class="mt-4 d-flex justify-content-between align-items-center">
                            <span>
                                <span class="card-title" id="heading">Nama Jurusan</span>
                                <div id="" class="d-none spinner-border spinner-border-sm text-success mx-3" role="status"></div>
                            </span>
                            <span id="" onclick="hide(this.parentNode.parentNode.parentNode.parentNode)" class="float-right text-danger"><i class="bi bi-trash"></i></span>
                        </h5>
                        <div class="row">
                            <div class="col-9">
                                <div class="form-group mb-3">
                                    <input type="text" class="form-control" id="icon" aria-describedby="emailHelp" placeholder="Icon" value="" onblur="postProgram(this.parentNode.parentNode.parentNode.parentNode)">
                                    <div class="invalid-feedback">
                                        Icon harus diisi
                                    </div>
                                </div>
                                <div class="form-group mb-3">
                                    <input type="text" class="form-control" id="title" aria-describedby="emailHelp" placeholder="Nama Jurusan" value="" onblur="postProgram(this.parentNode.parentNode.parentNode.parentNode)">
                                    <div class="invalid-feedback">
                                        Nama Jurusan harus diisi
                                    </div>
                                </div>
                            </div>
                            <div class="col-3 d-flex justify-content-center align-items-center">
                                <i id="icon-prev-" class="bi bi-question-circle" style="font-size: 50px;"></i>
                            </div>
                        </div>
                        <div class="form-group mb-3">
                            <textarea class="form-control" id="subtitle-" placeholder="Deskripsi" rows="5" onblur="postProgram(this.parentNode.parentNode)"></textarea>
                            <div class="invalid-feedback">
                                Deskripsi harus diisi
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        `.trim()
        rowContainer.insertBefore(div.firstChild, colPlus)
    }

    const hide = (e) => {
        e.classList.add('d-none')
    }

    const postProgram = (e) => {
        const icon = e.childNodes[3].childNodes[1].childNodes[1].childNodes[1]
        const title = e.childNodes[3].childNodes[1].childNodes[3].childNodes[1]
        const subtitle = e.childNodes[5].childNodes[1]
        const trash = e.childNodes[1].childNodes[3]
        const iconPrev = e.childNodes[3].childNodes[3].childNodes[1]
        const spinner = e.childNodes[1].childNodes[1].childNodes[3]
        const heading = e.childNodes[1].childNodes[1].childNodes[1]
        if (icon.value == "") icon.classList.add('is-invalid')
        else icon.classList.remove('is-invalid')
        if (title.value == "") title.classList.add('is-invalid')
        else title.classList.remove('is-invalid')
        if (subtitle.value == "") subtitle.classList.add('is-invalid')
        else subtitle.classList.remove('is-invalid')

        if (icon.value == "" || title.value == "" || subtitle.value == "") return false


        postData('/api/program-unggulan', {
                icon: icon.value,
                title: title.value,
                subtitle: subtitle.value,
                token: "<?= $_COOKIE['token'] ?>"
            }).then((data) => {
                if(data.error) throw data
                show(data.message)
                id = data.id
                heading.id = 'heading-' + id
                heading.textContent = title.value
                iconPrev.className = icon.value
                iconPrev.id = 'icon-prev-' + id
                spinner.id = 'spinner-' + id
                icon.id = 'icon-' + id
                icon.setAttribute('onblur', 'editIcon(this)')
                title.id = 'title-' + id
                title.setAttribute('onblur', 'editTitle(this)')
                subtitle.id = 'subtitle-' + id
                subtitle.setAttribute('onblur', 'editSubtitle(this)')
                trash.id = 'trash-' + id
                trash.setAttribute('onclick', 'deleteConfirm(this)')
            })
            .catch((err) => {
                console.log(err);
                show(err.error)

            })
    }
</script>
@endsection
