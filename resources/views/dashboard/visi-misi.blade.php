@extends('../template')
@section('title', 'Visi dan Misi')
@section('main')
<main id="main" class="main">

    <div class="pagetitle">
        <h1>Visi dan Misi</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                <li class="breadcrumb-item active">Visi dan Misi</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="section dashboard">
        <div class="row">

            <!-- Left side columns -->
            <div class="col-lg-6">
                <div class="card info-card sales-card">

                    <div class="card-body">
                        <h5 class="card-title">Visi
                            <div id="visi-spinner" class="d-none spinner-border spinner-border-sm text-success mx-3" role="status">
                                <span class="visually-hidden">Loading...</span>
                            </div>
                        </h5>
                        <div class="form-group mb-3">
                            <textarea class="form-control" id="visi" placeholder="Deskripsi" rows="5">{{ $visi->value }}</textarea>
                        </div>

                    </div>

                </div>
            </div><!-- End Left side columns -->

            <!-- Right side columns -->
            <div class="col-lg-6">

                <!-- Recent Activity -->
                <div class="card">
                    <div class="card-body" id="card-misi">
                        <h5 class="card-title">Misi
                            <div id="misi-spinner" class="d-none spinner-border spinner-border-sm text-success mx-3" role="status">
                                <span class="visually-hidden">Loading...</span>
                            </div>
                        </h5>
                        @foreach($misi as $m)
                        <div class="form-group mb-3">
                            <textarea class="form-control" id="{{ $m->id }}" placeholder="Deskripsi" rows="2" onblur="editMisi(this)">{{ $m->value }}</textarea>
                        </div>
                        @endforeach

                        <button class="btn btn-success" id="btn-tambah-misi">Tambah Misi</button>
                        <!-- <form action="/api/banner/image" method="post" enctype="multipart/form-data">
                @csrf
                <input type="file" name="data" id="">
                <button type="submit">kirim</button>
            </form> -->
                    </div>
                </div><!-- End Recent Activity -->

            </div><!-- End sidebar recent posts-->

        </div>
        </div><!-- End News & Updates -->

        </div><!-- End Right side columns -->

        </div>
    </section>

</main><!-- End #main -->

<script>
    const cardMisi = document.querySelector('#card-misi')
    const btnTambahMisi = document.querySelector('#btn-tambah-misi')
    const visiSpinner = document.querySelector('#visi-spinner')
    const misiSpinner = document.querySelector('#misi-spinner')
    const visi = document.querySelector('#visi')

    btnTambahMisi.addEventListener('click', (e) => {
        const div = document.createElement('div')
        div.classList.add('form-group')
        div.classList.add('mb-3')
        const textArea = document.createElement('textarea')
        textArea.classList.add('form-control')
        textArea.setAttribute('row', 2)
        textArea.setAttribute('onblur', 'addMisi(this)')
        div.appendChild(textArea)
        cardMisi.insertBefore(div, btnTambahMisi)
    })

    visi.addEventListener('blur', (e) => {
        visiSpinner.classList.remove('d-none')
        console.log({
            data: e.target.value
        });
        putData('api/visi-misi/visi/1', {
                data: e.target.value,
                token: "<?= $_COOKIE['token'] ?>"
            })
            .then((data) => {
                if(data.error) throw data
                show(data.message)
                visiSpinner.classList.add('d-none')
            })
            .catch((err) => {
                console.log(err);
                show(err.error)

                visiSpinner.classList.add('d-none')
            })

    })


    const editMisi = (e) => {
        console.log(e);
        misiSpinner.classList.remove('d-none')
        console.log({
            data: e.value
        });
        if (e.value == '' || e.value == null) return deleteMisi(e)
        putData(`api/visi-misi/misi/${e.id}`, {
                data: e.value,
                token: "<?= $_COOKIE['token'] ?>"
            })
            .then((data) => {
                if(data.error) throw data
                show(data.message)
                misiSpinner.classList.add('d-none')
            })
            .catch((err) => {
                console.log(err);
                show(err.error)

                misiSpinner.classList.add('d-none')
            })

    }

    const deleteMisi = (e) => {
        deleteData(`api/visi-misi/misi/${e.id}`, {
            token: "<?= $_COOKIE['token'] ?>"
        })
            .then((data) => {
                if(data.error) throw data
                show(data.message)
                misiSpinner.classList.add('d-none')
                e.classList.add('d-none')
            })
            .catch((err) => {
                console.log(err);
                show(err.error)

                misiSpinner.classList.add('d-none')
            })
    }

    const addMisi = (e) => {
        postData(`api/visi-misi/misi`, {
                data: e.value,
                token: "<?= $_COOKIE['token'] ?>"
            })
            .then((data) => {
                if(data.error) throw data
                show(data.message)
                misiSpinner.classList.add('d-none')
                e.id = data.id
                e.setAttribute('onblur', 'editMisi(this)')
            })
            .catch((err) => {
                console.log(err);
                show(err.error)

                misiSpinner.classList.add('d-none')
            })
    }
</script>

@endsection
