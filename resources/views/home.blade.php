@extends('template')
@section('title', 'Home')
@section('main')
<main id="main" class="main">
    <div class="pagetitle">
        <h1>Home</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Home</a></li>
            </ol>
        </nav>
    </div><!-- End Page Title -->
    <section class="section dashboard">
        <div class="row" id="row-container">
            <h4>Selamat datang {{$user->nama}}</h4>
            <p>Ini adalah halaman adminstrator website matsiba.com. Mohon untuk tidak memberikan akses masuk kepada sembarang guru/karyawan. Apabila terdapat kendala, silahkan hubungi developer</p>

        </div>
    </section>
</main>
@endsection
