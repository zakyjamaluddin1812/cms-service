@extends('../template')
@section('title', 'PPDB')
@section('main')
<main id="main" class="main">

    <div class="pagetitle">
        <h1>Edit Siswa</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                <li class="breadcrumb-item active">PPDB</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="section dashboard">
        <form action="/form/edit" method="post" enctype="multipart/form-data">
            @csrf
            <h3 class="mt-5">Identitas Siswa</h3>
            <input type="hidden" name="id" value="{{$siswa->id}}">
            <div class="mb-3">
                <label for="namaLengkap" class="form-label">Nama Lengkap <span class="text-danger">*</span></label>
                <input value="{{$siswa->nama_lengkap}}" type="text" name="nama_lengkap" class="form-control" id="namaLengkap" required>
            </div>
            <div class="d-flex">
                <div class="mb-3 flex-grow-1 me-2">
                    <label for="ttl" class="form-label">Tempat Lahir <span class="text-danger">*</span></label>
                    <input value="{{$siswa->tempat_lahir}}" type="text" name="tempat_lahir" class="form-control me-1" id="ttl" required>
                </div>
                <div class="mb-3 flex-grow-1 ms-2">
                    <label for="tgl" class="form-label">Tanggal Lahir <span class="text-danger">*</span></label>
                    <input value="{{$siswa->tanggal_lahir}}" type="date" name="tanggal_lahir" class="form-control ms-1" id="tgl" required>
                </div>
            </div>
            <div class="mb-3">
                <label for="sch" class="form-label">Asal Sekolah <span class="text-danger">*</span></label>
                <input value="{{$siswa->asal_sekolah}}" type="text" name="asal_sekolah" class="form-control" id="sch" required>
            </div>
            <div class="mb-3">
                <label for="nisn" class="form-label">Nomor Induk Siswa Nasional</label>
                <input value="{{$siswa->nisn}}" type="number" name="nisn" class="form-control" id="nisn">
            </div>
            <div class="mb-3">
                <label for="nik" class="form-label">Nomor Induk Kependudukan <span class="text-danger">*</span></label>
                <input value="{{$siswa->nik_siswa}}" type="number" name="nik_siswa" class="form-control" id="nik" required min="1000000000000000" max="9999999999999999">
            </div>
            <div class="mb-3">
                <label for="noKk" class="form-label">Nomor KK <span class="text-danger">*</span></label>
                <input value="{{$siswa->nomor_kk}}" type="number" name="nomor_kk" class="form-control" id="noKk" min="1000000000000000" max="9999999999999999" required>
            </div>

            <div class="mb-3">
                <label for="gender" class="form-label">Jenis Kelamin <span class="text-danger">*</span></label>
                <select class="form-select" name="jenis_kelamin" aria-label="Default select example" id="gender" required>
                    <option></option>
                    <option {{$siswa->jenis_kelamin == 'Laki-laki' ? 'selected' : ''}} value="Laki-laki">Laki-laki</option>
                    <option {{$siswa->jenis_kelamin == 'Perempuan' ? 'selected' : ''}} value="Perempuan">Perempuan</option>
                </select>
            </div>
            <div class="d-flex">
                <div class="mb-3 me-2 flex-grow-1">
                    <label for="anakKe" class="form-label">Anak Ke <span class="text-danger">*</span></label>
                    <input value="{{$siswa->anak_ke}}" type="number" name="anak_ke" min="1" max="15" class="form-control me-1" id="anakKe" required>
                </div>
                <div class="mb-3 ms-2 flex-grow-1">
                    <label for="saudara" class="form-label">Jumlah Saudara <span class="text-danger">*</span></label>
                    <input value="{{$siswa->jumlah_saudara}}" type="number" name="jumlah_saudara" min="0" max="15" class="form-control ms-1" id="saudara" required>
                </div>
            </div>
            <h3 class="mt-5">Alamat</h3>
            <div class="d-flex">
                <div class="mb-3 me-2 flex-grow-1">
                    <label for="rt" class="form-label">RT <span class="text-danger">*</span></label>
                    <input value="{{$siswa->rt}}" type="number" name="rt" min="0" max="100" class="form-control me-1" id="rt" required>
                </div>
                <div class="mb-3 ms-2 flex-grow-1">
                    <label for="rw" class="form-label">RW <span class="text-danger">*</span></label>
                    <input value="{{$siswa->rw}}" type="number" name="rw" min="0" max="100" class="form-control ms-1" id="rw" required>
                </div>
            </div>
            <div class="mb-3">
                <label for="desa" class="form-label">Desa <span class="text-danger">*</span></label>
                <input value="{{$siswa->desa}}" type="text" name="desa" class="form-control ms-1" id="desa" required>
            </div>
            <div class="mb-3">
                <label for="kecamatan" class="form-label">Kecamatan <span class="text-danger">*</span></label>
                <input value="{{$siswa->kecamatan}}" type="text" name="kecamatan" class="form-control ms-1" id="kecamatan" required>
            </div>
            <div class="mb-3">
                <label for="kabupaten" class="form-label">Kabupaten <span class="text-danger">*</span></label>
                <input value="{{$siswa->kabupaten}}" type="text" name="kabupaten" class="form-control ms-1" id="kabupaten" required>
            </div>
            <h3 class="mt-5">Identitas Orang Tua</h3>
            <div class="mb-3">
                <label for="namaAyah" class="form-label">Nama Ayah <span class="text-danger">*</span></label>
                <input value="{{$siswa->nama_ayah}}" type="text" name="nama_ayah" class="form-control" id="namaAyah" required>
            </div>
            <div class="mb-3">
                <label for="nikAyah" class="form-label">NIK Ayah <span class="text-danger">*</span></label>
                <input value="{{$siswa->nik_ayah}}" type="number" name="nik_ayah" min="1000000000000000" max="9999999999999999" class="form-control" id="nikAyah" required>
            </div>
            <div class="mb-3">
                <label for="kerjaAyah" class="form-label">Pekerjaan Ayah <span class="text-danger">*</span></label>
                <input value="{{$siswa->pekerjaan_ayah}}" type="text" name="pekerjaan_ayah" class="form-control" id="kerjaAyah" required>
            </div>
            <div class="mb-3">
                <label for="namaIbu" class="form-label">Nama Ibu <span class="text-danger">*</span></label>
                <input value="{{$siswa->nama_ibu}}" type="text" name="nama_ibu" class="form-control" id="namaIbu" required>
            </div>
            <div class="mb-3">
                <label for="nikIbu" class="form-label">NIK Ibu <span class="text-danger">*</span></label>
                <input value="{{$siswa->nik_ibu}}" type="number" name="nik_ibu" min="1000000000000000" max="9999999999999999" class="form-control" id="nikIbu" required>
            </div>
            <div class="mb-3">
                <label for="kerjaIbu" class="form-label">Pekerjaan Ibu <span class="text-danger">*</span></label>
                <input value="{{$siswa->pekerjaan_ibu}}" type="text" name="pekerjaan_ibu" class="form-control" id="kerjaIbu" required>
            </div>
            <h3 class="mt-5">Identitas Wali</h3>
            <div class="mb-3">
                <label for="namaWali" class="form-label">Nama Wali</label>
                <input value="{{$siswa->nama_wali}}" type="text" name="nama_wali" class="form-control" id="namaWali">
            </div>
            <div class="mb-3">
                <label for="nikWali" class="form-label">NIK Wali</label>
                <input value="{{$siswa->nik_wali}}" type="number" name="nik_wali" min="1000000000000000" max="9999999999999999" class="form-control" id="nikWali">
            </div>
            <div class="mb-3">
                <label for="kerjaWali" class="form-label">Pekerjaan Wali</label>
                <input value="{{$siswa->pekerjaan_wali}}" type="text" name="pekerjaan_wali" class="form-control" id="kerjaWali">
            </div>
            <h3 class="mt-5">Kontak</h3>
            <div class="mb-3">
                <label for="noHp" class="form-label">Nomor HP Orangtua / Wali</label>
                <input value="{{$siswa->nomor_hp}}" type="number" name="nomor_hp" class="form-control" id="noHp">
            </div>
            <h3 class="mt-5">Persyaratan Dokumen (JPG/PDF)</h3>
            <div class="mb-3">
                <label for="kk" class="form-label">Kartu Keluarga</label>
                <input type="file" name="file_kk" class="form-control" id="kk">
            </div>
            <div class="mb-3">
                <label for="ijazah" class="form-label">Ijazah SD/MI</label>
                <input type="file" name="file_ijazah" class="form-control" id="ijazah">
            </div>
            <div class="mb-3">
                <label for="nisn" class="form-label">Kartu NISN</label>
                <input type="file" name="file_nisn" class="form-control" id="nisn">
            </div>
            <div class="mb-3">
                <label for="kps" class="form-label">Kartu KPS (jika ada)</label>
                <input type="file" name="file_kps" class="form-control" id="kps">
            </div>
            <div class="mb-3">
                <label for="kps" class="form-label">Kartu PKH (jika ada)</label>
                <input type="file" name="file_pkh" class="form-control" id="kps">
            </div>
            <div class="mb-3">
                <label for="foto" class="form-label">Foto (JPG)</label>
                <input type="file" name="file_foto" class="form-control" id="foto">
            </div>
            <h3 class="mt-5">Lain-lain</h3>
            <div class="mb-3">
                <label for="rekom" class="form-label">Guru Perekom</label>
                <input value="{{$siswa->guru_perekom}}" type="text" name="guru_perekom" class="form-control" id="rekom">
            </div>
            <p>Keterangan : <br>Kolom yang wajib diisi ditandai dengan tanda bintang (<span class="text-danger">*</span>)</p>
            <div class="row">
                <div class="col-lg-3 col-sm-12">
                    <div class="d-grid gap-2">
                        <button type="submit" class="mt-5 btn btn-success">Simpan Perubahan</button>
                    </div>
                </div>
            </div>
        </form>
    </section>
</main>
@endsection
