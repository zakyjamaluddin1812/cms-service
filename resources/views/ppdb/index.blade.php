@extends('../template')
@section('title', 'PPDB')
@section('main')
<main id="main" class="main">

    <div class="pagetitle">
        <h1>PPDB</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                <li class="breadcrumb-item active">PPDB</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <button id="btn-form" {{$status->status == 'tutup' ? 'disabled' : ''}} onclick="window.location.href = '/form'" class="btn btn-success my-3">Tambah Data Siswa</button>
    <a href="" class="btn-outline-secondary btn" data-bs-toggle="modal" data-bs-target="#setting">
        <i id="icon" class="bi bi-gear text-success"></i>
        <div id="spinner" class="d-none spinner-border text-success spinner-border-sm" role="status">
            <span class="visually-hidden">Loading...</span>
        </div>
    </a>

    <section class="section dashboard">
        <table id="example" class="table table-striped" style="width:100%">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Asal Sekolah</th>
                </tr>
            </thead>
            <tbody>
                @foreach($siswa as $s)
                <tr id="row-{{$s->id}}" data-bs-toggle="modal" data-bs-target="#detail-{{$s->id}}">
                    <td>{{$s->id}}</td>
                    <td>{{$s->nama_lengkap}}</td>
                    <td>{{$s->asal_sekolah}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>

    </section>

</main><!-- End #main -->
<!-- Modal -->
<div class="modal fade" id="setting" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header d-flex justify-content-center">
                <h1 class="modal-title fs-5 text-center" id="exampleModalLabel">Pengaturan</h1>
            </div>
            <div class="modal-body">
                <div class="input-group mb-2">
                    <button class="btn btn-outline-secondary" type="button" id="inputGroupFileAddon03">Tahun Ajaran</button>
                    <input id="tahun1" {{$status->status == 'buka' ? 'disabled' : ''}} type="number" class="form-control" min=" 1900" max="2099" step="1" value="{{explode('/', $status->tahun_ajaran)[0]}}" onchange="tahunAjaran(this)" />
                    <input id="tahun2" type="number" class="form-control" min=" 1900" max="2099" step="1" value="{{explode('/', $status->tahun_ajaran)[1]}}" disabled />
                </div>
            </div>
            <div class="modal-footer d-flex justify-content-center">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                <button type="button" class="{{ $status->status == 'buka' ? 'd-none' : ''}} btn btn-success" onclick="ubahStatus('buka')" data-bs-dismiss="modal" id="btn-buka">Buka Pendaftaran</button>
                <button type="button" class="{{ $status->status == 'tutup' ? 'd-none' : ''}} btn btn-danger" onclick="ubahStatus('tutup')" data-bs-dismiss="modal" id="btn-tutup">Tutup Pendaftaran</button>
            </div>
        </div>
    </div>
</div>
@foreach($siswa as $s)
<div class="modal fade" id="detail-{{$s->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <div class="row">
                    <div class="col-4">
                        @if($s->file_foto != null)
                            @if(file_exists('assets/file/foto/' .$s->file_foto))
                            <img src="assets/file/foto/{{$s->file_foto}}" width="100%" class="rounded img-thumbnail" alt="">
                            @else
                            <img src="{{env('CLIENT')}}assets/file/foto/{{$s->file_foto}}" width="100%" class="rounded img-thumbnail" alt="">
                            @endif
                        @else
                        <img src="assets/img/foto/user.png" width="100%" class="rounded img-thumbnail" alt="">
                        @endif
                    </div>
                    <div class="col-8">
                        <h4 class=""><b>{{$s->nama_lengkap}}</b></h4>
                        <p><b>{{$s->asal_sekolah}}</b><br> Rt : {{$s->rt}} Rw : {{$s->rw}} Desa {{$s->desa}} Kecamatan {{$s->kecamatan}} Kabupaten {{$s->kabupaten}}</p>
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-4">TTL</div>
                    <div class="col-8">: {{$s->tempat_lahir}}, {{$s->tanggal_lahir}}</div>
                    <div class="col-4">NISN</div>
                    <div class="col-8">: {{$s->nisn}}</div>
                    <div class="col-4">NIK</div>
                    <div class="col-8">: {{$s->nik_siswa}}</div>
                    <div class="col-4">No. KK</div>
                    <div class="col-8">: {{$s->nomor_kk}}</div>
                    <div class="col-4">Jenis Kelamin</div>
                    <div class="col-8">: {{$s->jenis_kelamin}}</div>
                    <div class="col-4">Anak Ke</div>
                    <div class="col-8">: {{$s->anak_ke}}</div>
                    <div class="col-4">Jumlah Saudara</div>
                    <div class="col-8">: {{$s->jumlah_saudara}}</div>
                    <div class="col-4">Nama Ayah</div>
                    <div class="col-8">: {{$s->nama_ayah}}</div>
                    <div class="col-4">NIK Ayah</div>
                    <div class="col-8">: {{$s->nik_ayah}}</div>
                    <div class="col-4">Pekerjaan Ayah</div>
                    <div class="col-8">: {{$s->pekerjaan_ayah}}</div>
                    <div class="col-4">Nama Ibu</div>
                    <div class="col-8">: {{$s->pekerjaan_ibu}}</div>
                    <div class="col-4">NIK Ibu</div>
                    <div class="col-8">: {{$s->nik_ibu}}</div>
                    <div class="col-4">Pekerjaan Ibu</div>
                    <div class="col-8">: {{$s->pekerjaan_ibu}}</div>
                    <div class="col-4">Nama Wali</div>
                    <div class="col-8">: {{$s->nama_wali}}</div>
                    <div class="col-4">NIK Wali</div>
                    <div class="col-8">: {{$s->nik_wali}}</div>
                    <div class="col-4">Pekerjaan Wali</div>
                    <div class="col-8">: {{$s->pekerjaan_wali}}</div>
                    <div class="col-4">Nomor HP</div>
                    <div class="col-8">: {{$s->nomor_hp}}</div>
                    <div class="col-4">KK</div>
                    <div class="col-8">:
                        @if($s->file_kk != null)
                        @if(file_exists('assets/file/kk/' .$s->file_kk))
                        <a href="assets/file/kk/{{$s->file_kk}}">Klik untuk melihat</a>
                        @else
                        <a href="{{env('CLIENT')}}assets/file/kk/{{$s->file_kk}}">Klik untuk melihat</a>
                        @endif
                        @else
                        <a>Belum ada file</a>
                        @endif
                    </div>
                    <div class="col-4">Ijazah</div>
                    <div class="col-8">:
                        @if($s->file_ijazah != null)
                        @if(file_exists('assets/file/ijazah/' .$s->file_ijazah))
                        <a href="assets/file/ijazah/{{$s->file_ijazah}}">Klik untuk melihat</a>
                        @else
                        <a href="{{env('CLIENT')}}assets/file/ijazah/{{$s->file_ijazah}}">Klik untuk melihat</a>
                        @endif
                        @else
                        <a>Belum ada file</a>
                        @endif
                    </div>
                    <div class="col-4">NISN</div>
                    <div class="col-8">:
                        @if($s->file_nisn != null)
                        @if(file_exists('assets/file/nisn/' .$s->file_nisn))
                        <a href="assets/file/nisn/{{$s->file_nisn}}">Klik untuk melihat</a>
                        @else
                        <a href="{{env('CLIENT')}}assets/file/nisn/{{$s->file_nisn}}">Klik untuk melihat</a>
                        @endif
                        @else
                        <a>Belum ada file</a>
                        @endif
                    </div>
                    <div class="col-4">KPS</div>
                    <div class="col-8">:
                        @if($s->file_kps != null)
                        @if(file_exists('assets/file/kps/' .$s->file_kps))
                        <a href="assets/file/kps/{{$s->file_kps}}">Klik untuk melihat</a>
                        @else
                        <a href="{{env('CLIENT')}}assets/file/kps/{{$s->file_kps}}">Klik untuk melihat</a>
                        @endif
                        @else
                        <a>Belum ada file</a>
                        @endif
                    </div>
                    <div class="col-4">PKH</div>
                    <div class="col-8">:
                        @if($s->file_pkh != null)
                        @if(file_exists('assets/file/pkh/' .$s->file_pkh))
                        <a href="assets/file/pkh/{{$s->file_pkh}}">Klik untuk melihat</a>
                        @else
                        <a href="{{env('CLIENT')}}assets/file/pkh/{{$s->file_pkh}}">Klik untuk melihat</a>
                        @endif
                        @else
                        <a>Belum ada file</a>
                        @endif
                    </div>
                    <div class="col-4">Guru Perekom</div>
                    <div class="col-8">: {{$s->guru_perekom}}</div>
                </div>
            </div>
            <div class="modal-footer d-flex justify-content-center">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                <button id="btn-{{$s->id}}" type="button" class="btn btn-danger" data-bs-dismiss="modal" onclick="deleteConfirm(this)">Hapus</button>
                <a href="form/{{$s->id}}" class="btn btn-warning">Ubah</a>
            </div>
        </div>
    </div>
</div>
@endforeach

<script>
    const tahunAjaran = (e) => {
        const tahun2 = document.querySelector('#tahun2')
        tahun2.value = parseInt(e.value) + 1
    }

    const ubahStatus = (status) => {
        const tahun1 = document.querySelector('#tahun1')
        const tahun2 = document.querySelector('#tahun2')
        const btnBuka = document.querySelector('#btn-buka')
        const btnTutup = document.querySelector('#btn-tutup')
        const btnForm = document.querySelector('#btn-form')
        const icon = document.querySelector('#icon')
        const spinner = document.querySelector('#spinner')

        const tahun = tahun1.value + '/' + tahun2.value
        icon.classList.toggle('d-none')
        spinner.classList.toggle('d-none')
        putData('api/ppdb/status/', {
                tahun,
                status,
                token: "<?= $_COOKIE['token'] ?>"
            })
            .then((data) => {
                if (data.error) throw data
                show(data.message)
                btnBuka.classList.toggle('d-none')
                btnTutup.classList.toggle('d-none')
                icon.classList.toggle('d-none')
                spinner.classList.toggle('d-none')
                tahun1.toggleAttribute('disabled')
                btnForm.toggleAttribute('disabled')

            })
            .catch((err) => {
                show(err.error)
                spinner.classList.toggle('d-none')

            })

    }

    const deleteConfirm = (e) => {
        Swal.fire({
            title: 'Apakah anda yakin akan menghapus ini?',
            showCancelButton: true,
            confirmButtonText: 'Hapus',
            background: '#fff',
            color: '#000',
        }).then((result) => {
            if (result.isConfirmed) {
                deleteSiswa(e)
            }
        })
    }

    const deleteSiswa = (e) => {
        const id = e.id.split('-')[1]
        const icon = document.querySelector('#icon')
        const spinner = document.querySelector('#spinner')
        const row = document.querySelector('#row-' + id)
        icon.classList.toggle('d-none')
        spinner.classList.toggle('d-none')

        deleteData('<?= url('api/ppdb') ?>' + '/' + id, {
                token: "<?= $_COOKIE['token'] ?>"
            })
            .then((data) => {
                if (data.error) throw data
                show(data.message)
                icon.classList.toggle('d-none')
                spinner.classList.toggle('d-none')
                row.classList.add('d-none')
            })
            .catch((err) => {
                show(err.message)
                icon.classList.toggle('d-none')
                spinner.classList.toggle('d-none')
            })
    }
</script>
@endsection
