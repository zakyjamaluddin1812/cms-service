<?php

use App\Http\Controllers\Akun;
use App\Http\Controllers\Asrama;
use App\Http\Controllers\Auth;
use App\Http\Controllers\Banner;
use App\Http\Controllers\Beasiswa;
use App\Http\Controllers\Blog;
use App\Http\Controllers\Formulir;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Home;
use App\Http\Controllers\Kesiswaan;
use App\Http\Controllers\Kontak;
use App\Http\Controllers\Profil;
use App\Http\Controllers\ProgramUnggulan;
use App\Http\Controllers\VisiMisi;
use GuzzleHttp\Handler\Proxy;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('/test', [Home::class, 'test'])->middleware('validate_token');
Route::get('/render', [Home::class, 'render'])->middleware('validate_token');

// Route Banner
Route::get('/banner', [Banner::class, 'get'])->middleware('validate_token');
Route::post('/banner/image', [Banner::class, 'putImage'])->middleware('validate_token');
Route::put('/banner/title/{id}', [Banner::class, 'putTitle'])->middleware('validate_token');
Route::put('/banner/desc/{id}', [Banner::class, 'putDesc'])->middleware('validate_token');

// Profile Routes
Route::get('/profil', [Profil::class, 'get'])->middleware('validate_token');
Route::put('/profil/image', [Profil::class, 'putimage'])->middleware('validate_token');
Route::put('/profil/title', [Profil::class, 'putTitle'])->middleware('validate_token');
Route::put('/profil/desc', [Profil::class, 'putdesc'])->middleware('validate_token');
Route::put('/profil/list/icon/(:id)', [Profil::class, 'putListIcon'])->middleware('validate_token');
Route::put('/profil/list/title/(:id)', [Profil::class, 'putListTitle'])->middleware('validate_token');
Route::put('/profil/list/desc/(:id)', [Profil::class, 'putListDesc'])->middleware('validate_token');
Route::post('/profil/list', [Profil::class, 'postList'])->middleware('validate_token');
Route::delete('/profil/list/(:id)', [Profil::class, 'deleteList'])->middleware('validate_token');

// Visi Misi Routes
Route::put('/visi-misi/visi/{id}', [VisiMisi::class, 'putVisi'])->middleware('validate_token');
Route::put('/visi-misi/misi/{id}', [VisiMisi::class, 'putMisi'])->middleware('validate_token');
Route::delete('/visi-misi/misi/{id}', [VisiMisi::class, 'deleteMisi'])->middleware('validate_token');
Route::post('/visi-misi/misi/', [VisiMisi::class, 'addMisi'])->middleware('validate_token');

// Program Unggulan Routes
Route::put('/program-unggulan/title/{id}', [ProgramUnggulan::class, 'putTitle'])->middleware('validate_token');
Route::put('/program-unggulan/subtitle/{id}', [ProgramUnggulan::class, 'putSubtitle'])->middleware('validate_token');
Route::put('/program-unggulan/icon/{id}', [ProgramUnggulan::class, 'putIcon'])->middleware('validate_token');
Route::delete('/program-unggulan/{id}', [ProgramUnggulan::class, 'deleteProgram'])->middleware('validate_token');
Route::post('/program-unggulan', [ProgramUnggulan::class, 'postProgram'])->middleware('validate_token');

//Kesiswaan Routes
Route::post('/kesiswaan/image/{id}', [Kesiswaan::class, 'putImage'])->middleware('validate_token');
Route::post('/kesiswaan', [Kesiswaan::class, 'postKesiswaan'])->middleware('validate_token');
Route::put('/kesiswaan/title/{id}', [Kesiswaan::class, 'putTitle'])->middleware('validate_token');
Route::put('/kesiswaan/content/{id}', [Kesiswaan::class, 'putContent'])->middleware('validate_token');
Route::delete('/kesiswaan/{id}', [Kesiswaan::class, 'deleteKesiswaan'])->middleware('validate_token');

Route::post('/blog', [Blog::class, 'postBlog'])->middleware('validate_token');
Route::put('/blog/title/{id}', [Blog::class, 'putTitle'])->middleware('validate_token');
Route::post('/blog/image', [Blog::class, 'upImage']);
Route::post('/blog/image/{id}', [Blog::class, 'putImage'])->middleware('validate_token');
Route::put('/blog/deskripsi/{id}', [Blog::class, 'putDeskripsi'])->middleware('validate_token');
Route::put('/blog/content/{id}', [Blog::class, 'putContent'])->middleware('validate_token');
Route::post('/blog/content/{id}', [Blog::class, 'postContent'])->middleware('validate_token');
Route::delete('/blog/content/{id}', [Blog::class, 'deleteContent'])->middleware('validate_token');
Route::delete('/blog/{id}', [Blog::class, 'deleteBlog'])->middleware('validate_token');


Route::put('/asrama/title/{id}', [Asrama::class, 'putTitle'])->middleware('validate_token');

Route::put('/asrama/subtitle/{id}', [Asrama::class, 'putSubtitle'])->middleware('validate_token');
Route::post('/asrama/image/{id}', [Asrama::class, 'putImage'])->middleware('validate_token');
Route::put('/asrama/link/{id}', [Asrama::class, 'putLink'])->middleware('validate_token');
Route::post('/asrama', [Asrama::class, 'postAsrama'])->middleware('validate_token');
Route::delete('/asrama/{id}', [Asrama::class, 'deleteAsrama'])->middleware('validate_token');

//Beasiswa Routes
Route::post('/beasiswa/image/{id}', [Beasiswa::class, 'putImage'])->middleware('validate_token');
Route::post('/beasiswa', [Beasiswa::class, 'postBeasiswa'])->middleware('validate_token');
Route::put('/beasiswa/title/{id}', [Beasiswa::class, 'putTitle'])->middleware('validate_token');
Route::put('/beasiswa/content/{id}', [Beasiswa::class, 'putContent'])->middleware('validate_token');
Route::delete('/beasiswa/{id}', [Beasiswa::class, 'deleteBeasiswa'])->middleware('validate_token');

// Kontak Routes
Route::put('/kontak/key/{id}', [Kontak::class, 'putKey'])->middleware('validate_token');
Route::put('/kontak/value/{id}', [Kontak::class, 'putValue'])->middleware('validate_token');
Route::put('/kontak/icon/{id}', [Kontak::class, 'putIcon'])->middleware('validate_token');
Route::delete('/kontak/{id}', [Kontak::class, 'deleteKontak'])->middleware('validate_token');
Route::post('/kontak', [Kontak::class, 'postKontak'])->middleware('validate_token');

// PPdb Statuses Routes
Route::put('/ppdb/status', [Formulir::class, 'putStatus'])->middleware('validate_token');
Route::delete('/ppdb/{id}', [Formulir::class, 'deleteSiswa'])->middleware('validate_token');

Route::post('/akun', [Akun::class, 'postAkun'])->middleware('validate_token');
Route::delete('/akun/{id}', [Akun::class, 'deleteAkun'])->middleware('validate_token');
Route::put('/akun/{id}', [Akun::class, 'putAkun'])->middleware('validate_token');

// Auth
Route::post('/login', [Auth::class, 'login']);
