<?php

use App\Http\Controllers\Akun;
use App\Http\Controllers\Asrama;
use App\Http\Controllers\Auth;
use App\Http\Controllers\Banner;
use App\Http\Controllers\Beasiswa;
use App\Http\Controllers\Blog;
use App\Http\Controllers\Formulir;
use App\Http\Controllers\Home;
use App\Http\Controllers\Kesiswaan;
use App\Http\Controllers\Kontak;
use App\Http\Controllers\Pesan;
use App\Http\Controllers\ProgramUnggulan;
use App\Http\Controllers\VisiMisi;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [Home::class, 'index']);
Route::get('/banner', [Banner::class, 'index']);
Route::get('/visi-misi', [VisiMisi::class, 'index']);
Route::get('/program-unggulan', [ProgramUnggulan::class, 'index']);
Route::get('/kesiswaan', [Kesiswaan::class, 'index']);
Route::get('/blog', [Blog::class, 'index']);
Route::get('/blog/add', [Blog::class, 'add']);
Route::get('/blog/{id}', [Blog::class, 'detail']);

Route::get('/asrama', [Asrama::class, 'index']);
Route::get('/beasiswa', [Beasiswa::class, 'index']);
Route::get('/kontak', [Kontak::class, 'index']);


Route::post('/banner/image', [Banner::class, 'putImage']);

Route::get('/ppdb', [Formulir::class, 'index']);
Route::get('/form', [Formulir::class, 'form']);
Route::post('/form', [Formulir::class, 'addSiswa']);
Route::get('/form/{id}', [Formulir::class, 'formEdit']);
Route::post('/form/edit', [Formulir::class, 'putSiswa']);

Route::get('/login', [Auth::class, 'index']);
Route::get('/logout', [Auth::class, 'logout']);


Route::get('/akun', [Akun::class, 'index']);
Route::get('/pesan', [Pesan::class, 'index']);
